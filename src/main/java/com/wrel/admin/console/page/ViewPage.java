
package com.wrel.admin.console.page;

/**
 *
 * Page/Class Name: ViewPage
 * Title:
 * Description:
 * author: weiting
 * Create Date:	2016年4月3日
 * Last Modifier: eldar
 * Last Modify Date: 2016年4月3日
 * Version 1.0
 *
 */
public enum ViewPage {

    /**   公開頁面   **/
    PUBLIC_LOGIN("public/login", "/login"), //
    PUBLIC_REGISTER("public/register", "/register"), //

    /**   系統管理者頁面   **/
    ADMIN_USER("admin/user", "/adminUser"), //
    ADMIN_ROLE("admin/role", "/adminRole"), //
    ADMIN_FUNTION("admin/function", "/adminFunction"),

    /**  一般登入者頁面   **/
    WELCOME("welcome", "/welcome");

    private String viewPath;

    private String realUrl;

    ViewPage(final String viewPath, final String realUrl) {
        this.viewPath = viewPath;
        this.realUrl = realUrl;
    }

    public String getViewPath() {
        return this.viewPath;
    }

    public String getRealUrl() {
        return this.realUrl;
    }

}