
package com.wrel.admin.console.page;

import org.springframework.web.servlet.ModelAndView;

/**
 *
 * Page/Class Name: ModelAndViewFactory
 * Title:
 * Description:
 * author: weiting
 * Create Date:	2016年4月3日
 * Last Modifier: eldar
 * Last Modify Date: 2016年4月3日
 * Version 1.0
 *
 */
public class ModelAndViewFactory {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    public final static ModelAndView createModelAndView(final ViewPage viewPage) {
        if (viewPage == null) {
            throw new IllegalArgumentException("view page is null");
        }
        return new ModelAndView(viewPage.getViewPath());
    }
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
