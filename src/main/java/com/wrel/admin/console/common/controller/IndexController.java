
package com.wrel.admin.console.common.controller;

import static com.wrel.admin.console.page.ViewPage.PUBLIC_LOGIN;
import static com.wrel.admin.console.page.ViewPage.WELCOME;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.wrel.admin.console.common.AlertType;;

/**
 *
 * Page/Class Name: IndexController Title: Description: author: weiting Create
 * Date: 2016年3月26日 Last Modifier: eldar Last Modify Date: 2016年3月26日 Version
 * 1.0
 *
 */
@Controller
public class IndexController extends BaseController {
    // ================================================
    // == [Enumeration types] Block Start
    // ====

    // ====
    // == [Enumeration types] Block End
    // ================================================
    // == [static variables] Block Start
    // ====
    private final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    // ====
    // == [static variables] Block Stop
    // ================================================
    // == [instance variables] Block Start
    // ====

    // ====
    // == [instance variables] Block Stop
    // ================================================
    // == [static Constructor] Block Start
    // ====
    // ====
    // == [static Constructor] Block Stop
    // ================================================
    // == [Constructors] Block Start (含init method)
    // ====
    // ====
    // == [Constructors] Block Stop
    // ================================================
    // == [Static Method] Block Start
    // ====
    // ====
    // == [Static Method] Block Stop
    // ================================================
    // == [Accessor] Block Start
    // ====
    // ====
    // == [Accessor] Block Stop
    // ================================================
    // == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    // ====
    // ====
    // == [Overrided Method] Block Stop
    // ================================================
    // == [Method] Block Start
    // ====

    @RequestMapping(value = { "/", "index", "login" }, method = RequestMethod.GET)
    public ModelAndView index(@RequestParam(value = "error", required = false) String error, //
                              @RequestParam(value = "registed", required = false) String registed) {

        LOGGER.debug("index() is executed!");
        final ModelAndView model = this.getModelAndView(PUBLIC_LOGIN);

        if (error != null) {
            model.addObject("type", AlertType.DANGER.getValue());
            model.addObject("msg", "Invalid username and password!");
        }
        if (registed != null) {
            model.addObject("type", AlertType.SUCCESS.getValue());
            model.addObject("msg", "帳號申請完成，待驗證中...");
        }

        return model;
    }

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public ModelAndView welcome() {

        return this.getModelAndView(WELCOME);

    }

    // ####################################################################
    // ## [Method] sub-block :
    // ####################################################################
    // ====
    // == [Method] Block Stop
    // ================================================
}
