
package com.wrel.admin.console.common.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.wrel.admin.common.LoginInfo;
import com.wrel.admin.console.page.ModelAndViewFactory;
import com.wrel.admin.console.page.ViewPage;
import com.wrel.admin.entity.User;

/**
 *
 * Page/Class Name: BaseController
 * Title:
 * Description:
 * author: weiting
 * Create Date:	2016年3月28日
 * Last Modifier: eldar
 * Last Modify Date: 2016年3月28日
 * Version 1.0
 *
 */
public class BaseController {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    protected ModelAndView getModelAndView(ViewPage viewPage) {
        final ModelAndView model = ModelAndViewFactory.createModelAndView(viewPage);
        final User user = this.getLoginUser();
        if (user != null) {

            model.addObject("username", user.getName());
        }
        return model;
    }

    protected void setBasicInfo(Model model) {
        model.addAttribute("username", this.getUsername());
    }

    protected String getUsername() {
        final User user = this.getLoginUser();
        if (user != null) {

            return user.getName();
        } else {
            return StringUtils.EMPTY;
        }
    }

    protected final User getLoginUser() {
        Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (StringUtils.equals("anonymousUser", String.valueOf(obj))) {
            return null;
        } else {
            final LoginInfo loginInfo = (LoginInfo) obj;
            return loginInfo.getUser();
        }

    }
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
