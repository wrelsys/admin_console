
package com.wrel.admin.console.common;

/**
 *
 * Page/Class Name: AlertType
 * Title:
 * Description:
 * author: weiting
 * Create Date:	2016年4月5日
 * Last Modifier: eldar
 * Last Modify Date: 2016年4月5日
 * Version 1.0
 *
 */
public enum AlertType {
    DANGER("danger"),

    SUCCESS("success");
    private String value;

    AlertType(final String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

}
