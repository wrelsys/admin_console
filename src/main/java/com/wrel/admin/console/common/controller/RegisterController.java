/**
 * 
 */
package com.wrel.admin.console.common.controller;

import static com.wrel.admin.console.page.ViewPage.PUBLIC_REGISTER;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wrel.admin.console.request.RegisterRequestVO;
import com.wrel.admin.console.response.ExeResultVO;
import com.wrel.admin.console.service.UserService;

/**
 * @author 10503305
 *
 */
@Controller
public class RegisterController extends BaseController {

    private final Logger LOGGER = LoggerFactory.getLogger(RegisterController.class);

    @Autowired
    private UserService userService;

    /**
     * 註冊首頁
     * 
     * @return
     */
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView registerIndex() {
        return this.getModelAndView(PUBLIC_REGISTER);
    }

    /**
     * 
     * 註冊首頁送出頁
     * 
     * @param registerForm
     * @param result
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/api/register/add", method = RequestMethod.POST)
    public ExeResultVO<?> registerSave(@RequestBody RegisterRequestVO registerRequestVO) {

        LOGGER.debug("registerVO :{}", registerRequestVO.toString());

        return this.userService.registerNewUser(registerRequestVO);
    }

}
