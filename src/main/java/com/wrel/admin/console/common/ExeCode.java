
package com.wrel.admin.console.common;

/**
 *
 * Page/Class Name: ExeCode
 * Title:
 * Description:
 * author: weiting
 * Create Date:	2016年4月5日
 * Last Modifier: eldar
 * Last Modify Date: 2016年4月5日
 * Version 1.0
 *
 */
public enum ExeCode {
    /**執行失敗**/
    FAILURE(0),

    /**執行成功**/
    SUCCESS(1);

    private int code;

    ExeCode(final int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

}
