
package com.wrel.admin.console.common;

import java.io.Serializable;

/**
 *
 * Page/Class Name: SelectVO
 * Title:
 * Description:
 * Copyright: 
 * Company:	
 * author: 10503305 
 * Create Date:	2016年4月10日
 * Last Modify Date: 2016年4月10日
 * Version 1.0
 *
 */
public class OptionVO extends BasicVO implements Serializable {

    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    /**
     * <code>serialVersionUID</code> 的註解
     */
    private static final long serialVersionUID = 5845151659346484601L;

    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    private String label;

    private String value;

    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    public OptionVO() {

    }

    public OptionVO(final String lable, final String value) {
        this.label = lable;
        this.value = value;
    }

    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================

}
