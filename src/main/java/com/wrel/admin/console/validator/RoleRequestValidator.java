
package com.wrel.admin.console.validator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.wrel.admin.console.common.MsgVO;
import com.wrel.admin.console.request.RoleRequestVO;
import com.wrel.admin.entity.Role;
import com.wrel.admin.entity.Role.Status;

/**
 *
 * Page/Class Name: RoleRequestValidator
 * Title:
 * Description:
 * Copyright: 
 * Company:	President Information Corp.
 * author: Weiting
 * Create Date:	2016年4月4日 
 * Last Modify Date: 2016年4月4日
 * Version 1.0
 *
 */
public class RoleRequestValidator {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    public final static List<MsgVO> updateValidate(final RoleRequestVO requestVO, final Role role) {
        final List<MsgVO> msgs = new ArrayList<MsgVO>();
        if (role == null) {
            msgs.add(new MsgVO("id", "查無該角色"));
        }
        if (StringUtils.isBlank(requestVO.getRoleName())) {
            //角色名稱驗證
            msgs.add(new MsgVO("roleName", "角色名稱不能為空"));
        }
        { //角色狀態驗證
            if (requestVO.getStatus() == null) {
                msgs.add(new MsgVO("roleStatus", "請選擇角色狀態"));
            } else if (Status.lookup(requestVO.getStatus()) == null) {
                msgs.add(new MsgVO("roleStatus", "角色狀態錯誤，請重新選擇"));
            }
        }
        return msgs;
    }

    public final static List<MsgVO> validate(final RoleRequestVO requestVO, final Role role) {
        final List<MsgVO> msgs = new ArrayList<MsgVO>();
        {//角色代碼驗證
            if (StringUtils.isBlank(requestVO.getRoleId())) {
                msgs.add(new MsgVO("roleId", "角色代碼不能為空"));
            } else if (//
            role != null && //
                    StringUtils.isNotBlank(requestVO.getRoleId()) && //
                    requestVO.getRoleId() == role.getRoleId()) {
                msgs.add(new MsgVO("roleId", "角色代碼重覆"));
            }
        }

        if (StringUtils.isBlank(requestVO.getRoleName())) {
            //角色名稱驗證
            msgs.add(new MsgVO("roleName", "角色名稱不能為空"));
        }

        { //角色狀態驗證
            if (requestVO.getStatus() == null) {
                msgs.add(new MsgVO("roleStatus", "請選擇角色狀態"));
            } else if (Status.lookup(requestVO.getStatus()) == null) {
                msgs.add(new MsgVO("roleStatus", "角色狀態錯誤，請重新選擇"));
            }
        }
        return msgs;

    }
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
