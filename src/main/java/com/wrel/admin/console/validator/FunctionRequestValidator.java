
package com.wrel.admin.console.validator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.wrel.admin.console.common.MsgVO;
import com.wrel.admin.console.request.FunctionRequestVO;
import com.wrel.admin.entity.Function;
import com.wrel.admin.entity.Function.Show;
import com.wrel.admin.entity.Role.Status;

/**
 *
 * Page/Class Name: FunctionValidator
 * Title:
 * Description:
 * Copyright: 
 * Company:	
 * author: 10503305 
 * Create Date:	2016年4月10日
 * Last Modify Date: 2016年4月10日
 * Version 1.0
 *
 */
public class FunctionRequestValidator {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    public static final  List<MsgVO> updateValidate(final FunctionRequestVO functionRequestVO, final Function function) {
        final List<MsgVO> msgs = new ArrayList<MsgVO>();

        if (function == null) {
            msgs.add(new MsgVO("id", "查無該功能"));
        }

        { //功能名稱驗證
            if (StringUtils.isBlank(functionRequestVO.getFunctionName())) {

                msgs.add(new MsgVO("functionName", "功能名稱不能為空"));
            } else if (StringUtils.isNotBlank(functionRequestVO.getFunctionName()) && //
                    StringUtils.length(functionRequestVO.getFunctionName()) > 20) {
                msgs.add(new MsgVO("functionName", "功能名稱長度不能超過20"));
            }

        }

        {

            //功能URL 驗證
            if (StringUtils.isNotBlank(functionRequestVO.getFunctionUrl()) && //
                    StringUtils.length(functionRequestVO.getFunctionUrl()) > 50) {
                msgs.add(new MsgVO("functionUrl", "功能URL長度不能超過50"));
            }
        }

        { //功能狀態驗證
            if (functionRequestVO.getStatus() == null) {
                msgs.add(new MsgVO("functionStatus", "請選擇功能狀態"));
            } else if (Status.lookup(functionRequestVO.getStatus()) == null) {
                msgs.add(new MsgVO("functionStatus", "功能狀態錯誤，請重新選擇"));
            }
        }

        { //排序驗證
            if (StringUtils.isNotBlank(functionRequestVO.getOrder())
                    && !NumberUtils.isDigits(functionRequestVO.getOrder())) {
                msgs.add(new MsgVO("order", "排序欄位必須為數字"));
            }
        }
        {//是否顯示

            final Show show = Show.lookup(functionRequestVO.getShow());
            if (show == null) {
                msgs.add(new MsgVO("show", "請選擇功能是否呈現"));
            }
        }

        return msgs;
    }

    public static final  List<MsgVO> validate(final FunctionRequestVO functionRequestVO) {
        final List<MsgVO> msgs = new ArrayList<MsgVO>();
        { //功能名稱驗證
            if (StringUtils.isBlank(functionRequestVO.getFunctionName())) {

                msgs.add(new MsgVO("functionName", "功能名稱不能為空"));
            } else if (StringUtils.isNotBlank(functionRequestVO.getFunctionName()) && //
                    StringUtils.length(functionRequestVO.getFunctionName()) > 20) {
                msgs.add(new MsgVO("functionName", "功能名稱長度不能超過20"));
            }

        }

        {

            //功能URL 驗證
            if (StringUtils.isNotBlank(functionRequestVO.getFunctionUrl()) && //
                    StringUtils.length(functionRequestVO.getFunctionUrl()) > 50) {
                msgs.add(new MsgVO("functionUrl", "功能URL長度不能超過50"));
            }
        }

        { //功能狀態驗證
            if (functionRequestVO.getStatus() == null) {
                msgs.add(new MsgVO("functionStatus", "請選擇功能狀態"));
            } else if (Status.lookup(functionRequestVO.getStatus()) == null) {
                msgs.add(new MsgVO("functionStatus", "功能狀態錯誤，請重新選擇"));
            }
        }

        { //排序驗證
            if (StringUtils.isNotBlank(functionRequestVO.getOrder())
                    && !NumberUtils.isDigits(functionRequestVO.getOrder())) {
                msgs.add(new MsgVO("order", "排序欄位必須為數字"));
            }
        }
        {//是否顯示

            final Show show = Show.lookup(functionRequestVO.getShow());
            if (show == null) {
                msgs.add(new MsgVO("show", "請選擇功能是否呈現"));
            }
        }

        return msgs;
    }
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
