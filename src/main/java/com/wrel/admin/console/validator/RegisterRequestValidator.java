
package com.wrel.admin.console.validator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.EmailValidator;

import com.wrel.admin.console.common.MsgVO;
import com.wrel.admin.console.request.RegisterRequestVO;
import com.wrel.admin.entity.User;

/**
 *
 * Page/Class Name: RegisterFormValidator Title: Description: author: weiting
 * Create Date: 2016年4月3日 Last Modifier: eldar Last Modify Date: 2016年4月3日
 * Version 1.0
 *
 */
public class RegisterRequestValidator {
    // ================================================
    // == [Enumeration types] Block Start
    // ====
    // ====
    // == [Enumeration types] Block End
    // ================================================
    // == [static variables] Block Start
    // ====
    // ====
    // == [static variables] Block Stop
    // ================================================
    // == [instance variables] Block Start
    // ====
    // ====
    // == [instance variables] Block Stop
    // ================================================
    // == [static Constructor] Block Start
    // ====
    // ====
    // == [static Constructor] Block Stop
    // ================================================
    // == [Constructors] Block Start (含init method)
    // ====
    // ====
    // == [Constructors] Block Stop
    // ================================================
    // == [Static Method] Block Start
    // ====

    public final static List<MsgVO> validate(final RegisterRequestVO registerRequestVO, final User user) {
        final List<MsgVO> msgs = new ArrayList<MsgVO>();
        { //姓名驗證
            if (StringUtils.isBlank(registerRequestVO.getName())) {

                msgs.add(new MsgVO("name", "姓名不能為空"));
            } else if (StringUtils.isNotBlank(registerRequestVO.getName()) && //
                    StringUtils.length(registerRequestVO.getName()) > 5) {
                msgs.add(new MsgVO("name", "姓名長度不能超過5"));
            }

        }

        {

            //email 驗證
            if (StringUtils.isBlank(registerRequestVO.getEmail())) {
                msgs.add(new MsgVO("email", "email不能為空"));
            } else if (StringUtils.isNotBlank(registerRequestVO.getEmail()) && //
                    !EmailValidator.getInstance().isValid(registerRequestVO.getEmail())) {
                msgs.add(new MsgVO("email", "email格式錯誤"));
            } else if (StringUtils.isNotBlank(registerRequestVO.getEmail()) && //
                    user != null && //
                    StringUtils.equals(user.getEmail(), registerRequestVO.getEmail())) {
                msgs.add(new MsgVO("email", "email已被使用"));
            }
        }

        {
            //mobile 驗證

            if (StringUtils.isNotBlank(registerRequestVO.getMobile()) && //
                    StringUtils.length(registerRequestVO.getMobile()) > 15) {
                msgs.add(new MsgVO("mobile", "手機長度不能超過15"));
            }

        }

        {
            //tel 驗證
            if (StringUtils.isBlank(registerRequestVO.getTel())) {
                msgs.add(new MsgVO("tel", "連絡電話不能為空"));
            } else if (StringUtils.isNotBlank(registerRequestVO.getTel()) && //
                    StringUtils.length(registerRequestVO.getTel()) > 10) {
                msgs.add(new MsgVO("tel", "連絡電話長度不能超過15"));

            }
        }

        {
            //密碼驗證

            if (!StringUtils.equals(registerRequestVO.getPassword(), registerRequestVO.getRePassword())) {
                msgs.add(new MsgVO("rePassword", "請再次檢查密碼"));
            } else {
                if (StringUtils.isBlank(registerRequestVO.getPassword())) {
                    msgs.add(new MsgVO("password", "密碼不得為空"));
                } else if (StringUtils.isNotBlank(registerRequestVO.getPassword()) && //
                        (StringUtils.length(registerRequestVO.getPassword()) > 15 && //
                                StringUtils.length(registerRequestVO.getPassword()) < 8)) {
                    msgs.add(new MsgVO("password", "密碼長度需介於8~15"));
                }

                if (StringUtils.isBlank(registerRequestVO.getRePassword())) {
                    msgs.add(new MsgVO("rePassword", "密碼不得為空"));
                } else if (StringUtils.isNotBlank(registerRequestVO.getRePassword()) && //
                        (StringUtils.length(registerRequestVO.getRePassword()) > 15 && //
                                StringUtils.length(registerRequestVO.getRePassword()) < 8)) {
                    msgs.add(new MsgVO("rePassword", "密碼長度需介於8~15"));
                }
            }

        }

        return msgs;
    }
    // ====
    // == [Static Method] Block Stop
    // ================================================
    // == [Accessor] Block Start
    // ====
    // ====
    // == [Accessor] Block Stop
    // ================================================
    // == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    // ====
    // ====
    // == [Overrided Method] Block Stop
    // ================================================
    // == [Method] Block Start
    // ====
    // ####################################################################
    // ## [Method] sub-block :
    // ####################################################################
    // ====
    // == [Method] Block Stop
    // ================================================

}
