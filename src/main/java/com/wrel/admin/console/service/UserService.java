
package com.wrel.admin.console.service;

import java.util.List;

import com.wrel.admin.console.request.RegisterRequestVO;
import com.wrel.admin.console.request.UserQueryRequestVO;
import com.wrel.admin.console.response.ExeResultVO;
import com.wrel.admin.entity.User;
import com.wrel.admin.entity.User.Status;

/**
 *
 * Page/Class Name: UserService Title: Description: author: weiting Create Date:
 * 2016年3月26日 Last Modifier: eldar Last Modify Date: 2016年3月26日 Version 1.0
 *
 */
public interface UserService {
    // ================================================
    // == [Method] Block Start
    // ====
    User getUserByEmailAndStatus(final String email, final Status status);

    User getUserById(final Long id);

    ExeResultVO<?> registerNewUser(final RegisterRequestVO registerRequestVO);

    List<User> queryUser(final UserQueryRequestVO userQueryRequestVO);
    // ####################################################################
    // ## [Method] sub-block :
    // ####################################################################
    // ====
    // == [Method] Block Stop
    // ================================================
}
