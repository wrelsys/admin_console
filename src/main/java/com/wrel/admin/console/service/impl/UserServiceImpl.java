
package com.wrel.admin.console.service.impl;

import static com.wrel.admin.console.common.ExeCode.FAILURE;
import static com.wrel.admin.console.common.ExeCode.SUCCESS;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.wrel.admin.console.common.MsgVO;
import com.wrel.admin.console.entity.factory.UserFactory;
import com.wrel.admin.console.request.RegisterRequestVO;
import com.wrel.admin.console.request.UserQueryRequestVO;
import com.wrel.admin.console.response.ExeResultVO;
import com.wrel.admin.console.service.UserService;
import com.wrel.admin.console.validator.RegisterRequestValidator;
import com.wrel.admin.dao.UserDao;
import com.wrel.admin.entity.User;
import com.wrel.admin.entity.User.Status;

/**
 *
 * Page/Class Name: UserServiceImpl Title: Description: author: weiting Create
 * Date: 2016年3月26日 Last Modifier: eldar Last Modify Date: 2016年3月26日 Version
 * 1.0
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    // ================================================
    // == [Enumeration types] Block Start
    // ====
    // ====
    // == [Enumeration types] Block End
    // ================================================
    // == [static variables] Block Start
    // ====
    // ====
    // == [static variables] Block Stop
    // ================================================
    // == [instance variables] Block Start
    // ====
    @Autowired
    private UserDao userDao;

    @Value("${register.subject}")
    private String subject;

    @Value("${register.content}")
    private String content;

    @Value("${register.footer}")
    private String footer;

    // ====
    // == [instance variables] Block Stop
    // ================================================
    // == [static Constructor] Block Start
    // ====
    // ====
    // == [static Constructor] Block Stop
    // ================================================
    // == [Constructors] Block Start (含init method)
    // ====
    // ====
    // == [Constructors] Block Stop
    // ================================================
    // == [Static Method] Block Start
    // ====
    // ====
    // == [Static Method] Block Stop
    // ================================================
    // == [Accessor] Block Start
    // ====
    // ====
    // == [Accessor] Block Stop
    // ================================================
    // == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    // ====
    /**
     * 實作「 getUserByEmail 」方法
     * 
     * 
     * @see com.wrel.admin.service.UserService#getUserByEmail(java.lang.String)
     */
    @Override

    public final User getUserByEmailAndStatus(final String email, final Status status) {

        if (StringUtils.isBlank(email)) {
            return null;
        }
        return this.userDao.getUserByEmailAndStatus(email, status);
    }

    /**
     * 
     */
    @Override
    public final ExeResultVO<?> registerNewUser(final RegisterRequestVO registerRequestVO) {
        ExeResultVO<?> exeResult = null; 

        final List<MsgVO> msgs = RegisterRequestValidator.validate(registerRequestVO,
                this.getUserByEmailAndStatus(registerRequestVO.getEmail(), null));
        if (CollectionUtils.isNotEmpty(msgs)) {
            exeResult = new ExeResultVO<>(FAILURE, StringUtils.EMPTY, null, msgs);
        } else {
            final User user = UserFactory.getNewUserInstance(registerRequestVO);
            this.userDao.save(user);
            final String replaceContent = StringUtils.replace(content, "@email", user.getEmail()) + footer;
            // this.emailUtils.sendMail(user.getEmail(), subject, replaceContent);
            exeResult = new ExeResultVO<>(SUCCESS, "已完成註冊，請耐心等候資格審核", null, null);
        }

               return exeResult;
    }

    @Override
    public final List<User> queryUser(final UserQueryRequestVO userQueryRequestVO) {
        final User queryUser = UserFactory.getQueryUserInstance(userQueryRequestVO);
        final List<User> users = this.userDao.queryUser(queryUser);
        return users;
    }

    @Override
    public final User getUserById(final Long id) {
        if (id == null) {
            return null;
        } else {
            return this.userDao.get(User.class, id);
        }

    }
    // ====
    // == [Overrided Method] Block Stop
    // ================================================
    // == [Method] Block Start
    // ====
    // ####################################################################
    // ## [Method] sub-block :
    // ####################################################################
    // ====
    // == [Method] Block Stop
    // ================================================

}
