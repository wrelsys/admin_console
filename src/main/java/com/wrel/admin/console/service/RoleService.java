
package com.wrel.admin.console.service;

import java.util.List;

import com.wrel.admin.console.request.RoleRequestVO;
import com.wrel.admin.entity.Role;

/**
 *
 * Page/Class Name: RoleService
 * Title:
 * Description:
 * Copyright: 
 * Company:	President Information Corp.
 * author: Weiting
 * Create Date:	2016年4月4日 
 * Last Modify Date: 2016年4月4日
 * Version 1.0
 *
 */
public interface RoleService {
    //================================================
    //== [Method] Block Start
    //====

    List<Role> getAllRoles();

    void insertRole(final RoleRequestVO roleRequestVO);

    void updateRole(final RoleRequestVO roleRequestVO, final Role role) throws IllegalArgumentException;

    Role getRoleByRoleId(final String roleId);

    Role getRoleById(final Long id);
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
