
package com.wrel.admin.console.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wrel.admin.console.entity.factory.RoleFactory;
import com.wrel.admin.console.request.RoleRequestVO;
import com.wrel.admin.console.service.RoleService;
import com.wrel.admin.dao.RoleDao;
import com.wrel.admin.entity.Role;

/**
 *
 * Page/Class Name: RoleServiceImpl
 * Title:
 * Description:
 * Copyright: 
 * Company:	President Information Corp.
 * author: Weiting
 * Create Date:	2016年4月4日 
 * Last Modify Date: 2016年4月4日
 * Version 1.0
 *
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    @Autowired
    private RoleDao roleDao;

    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    /**
     * 實作「 取得所有的 Role  」方法
     * 
    
     * @see com.wrel.admin.console.service.RoleService#getAllRoles()
     */
    @Override
    public final List<Role> getAllRoles() {

        return this.roleDao.getAllRoles();
    }

    /**
     * 實作「 新增Role  」方法
     * 
    
     * @see com.wrel.admin.console.service.RoleService#insertRole(com.wrel.admin.entity.Role)
     */
    @Override
    public final void insertRole(final RoleRequestVO roleRequestVO)

    {

        this.roleDao.save(RoleFactory.getNewRoleInstance(roleRequestVO));

    }

    /**
     * 
     * 實作「 取得Role by role code  」方法
     * 
    
     * @see com.wrel.admin.console.service.RoleService#getRoleByRoleCode(java.lang.String)
     */
    @Override
    public final Role getRoleByRoleId(final String roleId) {
        if (StringUtils.isBlank(roleId)) {
            return null; 
        }

        return this.roleDao.getRoleByRoleId(roleId);
    }

    /**
     * 
     * 
     */
    @Override
    public final Role getRoleById(final Long id) {
        if (id == null) {
            return null;
        }

        return this.roleDao.get(Role.class, id);
    }

    @Override
    public final void updateRole(final RoleRequestVO roleRequestVO, final Role role) throws IllegalArgumentException

    {
        if (role == null) {
            throw new IllegalArgumentException("role id : " + roleRequestVO.getId() + " is not exist");
        } else {
            role.setRoleName(roleRequestVO.getRoleName());
            role.setStatus(roleRequestVO.getStatus());
            role.setModifyDate(new Date());
            this.roleDao.update(role);
        }
    }

    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
