
package com.wrel.admin.console.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.wrel.admin.console.common.OptionVO;
import com.wrel.admin.console.entity.factory.FunctionFactory;
import com.wrel.admin.console.request.FunctionRequestVO;
import com.wrel.admin.console.service.FunctionService;
import com.wrel.admin.dao.FunctionDao;
import com.wrel.admin.entity.Function;
import com.wrel.admin.entity.Function.Status;

/**
 *
 * Page/Class Name: FunctionServiceImpl
 * Title:
 * Description:
 * Copyright: 
 * Company:	
 * author: 10503305 
 * Create Date:	2016年4月9日
 * Last Modify Date: 2016年4月9日
 * Version 1.0
 *
 */
@Service("functionService")
@Transactional
public class FunctionServiceImpl implements FunctionService {

    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    private final Logger LOGGER = LoggerFactory.getLogger(FunctionServiceImpl.class);

    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    @Autowired
    private FunctionDao functionDao;

    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    /**
     * 實作「 insertFunction  」方法
     * 
    
     * @see com.wrel.admin.console.service.FunctionService#insertFunction(com.wrel.admin.entity.Function)
     */
    @Override
    public void insertFunction(final FunctionRequestVO functionRequestVO)

    {

        final Function function = FunctionFactory.getNewFunctionInstance(functionRequestVO);

        if (functionRequestVO.getParentId() != null) {
            function.setParent(this.getFunctionIsParent(functionRequestVO.getParentId()));
        }

        this.functionDao.save(function);

    }

    /**
     * 
     * 實作「 getAllFunctions  」方法
     * 
    
     * @see com.wrel.admin.console.service.FunctionService#getAllFunctions()
     */
    @Override
    public List<Function> getAllFunctions() {

        final List<Function> functions = this.functionDao.getAllFunctions(true);
        LOGGER.debug("function :{}", new Gson().toJson(functions));
        return functions; 
    }

    /**
     * 
     * 實作「 getFunctionSelects  」方法
     * 
    
     * @see com.wrel.admin.console.service.FunctionService#getFunctionSelects(java.util.List)
     */
    @Override
    public List<OptionVO> getFunctionSelects(List<Function> functions) {
        final List<OptionVO> selects = new ArrayList<OptionVO>();

        if (functions == null) {
            functions = this.functionDao.getAllFunctions(false);
        }
        if (CollectionUtils.isNotEmpty(functions)) {
            for (Function function : functions) {
                selects.add(new OptionVO(function.getFunctionName(), String.valueOf(function.getId())));
            }

        }
        return selects;
    }

    @Override
    public Function getFunctionIsParent(final Long parentId) {
        if (parentId == null) {
            return null;
        }
        return this.functionDao.getFunctionIsParent(parentId);
    }

    @Override
    public Function getFunctionById(final Long id) {

        if (id == null) {
            return null;
        } else {

            return this.functionDao.getFunctionById(id);
        }

    }

    @Override
    public void updateFunction(final FunctionRequestVO functionRequestVO, final Function function)
            throws IllegalArgumentException {
        if (function == null) { 
            throw new IllegalArgumentException("function id : " + functionRequestVO.getId() + " is not exist");
        } else {
            function.setFunctionName(functionRequestVO.getFunctionName());
            function.setFunctionUrl(functionRequestVO.getFunctionUrl());
            function.setOrder(Integer.valueOf(functionRequestVO.getOrder()));
            function.setStatus(Status.lookup(functionRequestVO.getStatus()).getValue());
            function.setOrder(Integer.valueOf(functionRequestVO.getOrder()));
            function.setModifyDate(new Date());
            if (functionRequestVO.getParentId() != null) {
                function.setParent(this.getFunctionById(functionRequestVO.getParentId()));
            } else {
                function.setParent(null);
            }
            this.functionDao.update(function);
        }

    }

    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block :
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================

}
