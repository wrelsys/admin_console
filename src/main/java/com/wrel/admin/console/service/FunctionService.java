
package com.wrel.admin.console.service;

import java.util.List;

import com.wrel.admin.console.common.OptionVO;
import com.wrel.admin.console.request.FunctionRequestVO;
import com.wrel.admin.entity.Function;

/**
 *
 * Page/Class Name: FunctionService
 * Title:
 * Description:
 * Copyright: 
 * Company:	
 * author: 10503305 
 * Create Date:	2016年4月9日
 * Last Modify Date: 2016年4月9日
 * Version 1.0
 *
 */
public interface FunctionService {
    //================================================
    //== [Method] Block Start
    /**
     * Insert function.
     *
     * @param functionRequestVO the function request VO
     */
    //====
    void insertFunction(final FunctionRequestVO functionRequestVO);

    /**
     * Gets the function selects.
     *
     * @param functions the functions
     * @return the function selects
     */
    List<OptionVO> getFunctionSelects(List<Function> functions);

    /**
     * Update function.
     *
     * @param functionRequestVO the function request VO
     * @param function the function
     * @throws IllegalArgumentException the illegal argument exception
     */
    void updateFunction(final FunctionRequestVO functionRequestVO, final Function function)
            throws IllegalArgumentException;

    /**
     * Gets the function by id.
     *
     * @param id the id
     * @return the function by id
     */
    Function getFunctionById(Long id);

    /**
     * Gets the function is parent.
     *
     * @param parentId the parent id
     * @return the function is parent
     */
    Function getFunctionIsParent(Long parentId);

    /**
     * Gets the all functions.
     *
     * @return the all functions
     */
    List<Function> getAllFunctions();
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
