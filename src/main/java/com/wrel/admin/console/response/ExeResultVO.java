
package com.wrel.admin.console.response;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wrel.admin.console.common.ExeCode;
import com.wrel.admin.console.common.MsgVO;
import com.wrel.admin.console.common.OptionVO;

/**
 *
 * Page/Class Name: ExeResultVO
 * Title:
 * Description:
 * author: weiting
 * Create Date:	2016年4月5日
 * Last Modifier: eldar
 * Last Modify Date: 2016年4月5日
 * Version 1.0
 *
 */
public class ExeResultVO<T> implements Serializable {

    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    /**
     * <code>serialVersionUID</code> 的註解
     */
    private static final long serialVersionUID = 1144550487965602215L;

    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    private int exeCode;

    private T obj;

    private List<T> datas;

    private List<MsgVO> errorMsgs;

    private String successMsg;

    private String msg;

    private List<OptionVO> options;

    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    public ExeResultVO(final ExeCode exeCode, final String successMsg, final List<T> datas,
            final List<MsgVO> errorMsgs) {
        this.exeCode = exeCode.getCode();
        this.successMsg = successMsg;
        this.datas = datas;
        this.errorMsgs = errorMsgs;
    }

    public ExeResultVO(final ExeCode exeCode, final T obj, final String msg) {
        this.exeCode = exeCode.getCode();
        this.obj = obj;
        this.msg = msg;

    }

    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getObj() {
        return this.obj;
    }

    public void setObj(T obj) {
        this.obj = obj;
    }

    public List<OptionVO> getOptions() {
        return this.options;
    }

    public void setOptions(List<OptionVO> options) {
        this.options = options;
    }

    public List<T> getDatas() {
        return this.datas;
    }

    public int getExeCode() {
        return this.exeCode;
    }

    public void setExeCode(int exeCode) {
        this.exeCode = exeCode;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    public List<MsgVO> getErrorMsgs() {
        return this.errorMsgs;
    }

    public void setErrorMsgs(List<MsgVO> errorMsgs) {
        this.errorMsgs = errorMsgs;
    }

    public String getSuccessMsg() {
        return this.successMsg;
    }

    public void setSuccessMsg(String successMsg) {
        this.successMsg = successMsg;
    }

    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================

}
