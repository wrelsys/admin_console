
package com.wrel.admin.console.admin.controller;

import static com.wrel.admin.console.common.ExeCode.FAILURE;
import static com.wrel.admin.console.common.ExeCode.SUCCESS;
import static com.wrel.admin.console.page.ViewPage.ADMIN_ROLE;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wrel.admin.console.common.ExeCode;
import com.wrel.admin.console.common.MsgVO;
import com.wrel.admin.console.common.controller.BaseController;
import com.wrel.admin.console.request.RoleRequestVO;
import com.wrel.admin.console.response.ExeResultVO;
import com.wrel.admin.console.service.RoleService;
import com.wrel.admin.console.validator.RoleRequestValidator;
import com.wrel.admin.entity.Role;

/**
 *
 * Page/Class Name: RoleController
 * Title:
 * Description:
 * Copyright: 
 * Company:	President Information Corp.
 * author: Weiting
 * Create Date:	2016年4月4日 
 * Last Modify Date: 2016年4月4日
 * Version 1.0
 *
 */
@Controller
public class RoleController extends BaseController {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    private final Logger LOGGER = LoggerFactory.getLogger(RoleController.class);

    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    @Autowired
    private RoleService roleService;

    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    /**
     * 
     * admin新增角色初始頁面
     * 
     * @return
     */
    @RequestMapping(value = "/adminRole", method = RequestMethod.GET)
    public ModelAndView roleIndex() {

        final ModelAndView modelAndView = this.getModelAndView(ADMIN_ROLE);
        modelAndView.addObject("roles", this.roleService.getAllRoles());
        modelAndView.addObject("statuses", Role.Status.values());

        return modelAndView;

    }

    /**
     * 
     * RestFul API取得Role
     * 
     * @param id
     * @return
     */

    @ResponseBody
    @RequestMapping(value = "/api/adminRole/get/{id}", method = RequestMethod.GET)
    public ExeResultVO<Role> getRoleById(@PathVariable Long id) {

        LOGGER.debug("role id :{}", id);
        final Role role = this.roleService.getRoleById(id);
        ExeResultVO<Role> exeResult = null;
        if (role == null) {
            exeResult = new ExeResultVO<Role>(ExeCode.FAILURE, null, "查詢失敗");
        } else {
            exeResult = new ExeResultVO<Role>(ExeCode.SUCCESS, role, StringUtils.EMPTY);
        }
        return exeResult;
    }

    /**
     * 
     * RestFul API add new role
     * 
     * @param roleRequestVO
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/api/adminRole/add", method = RequestMethod.POST)
    public ExeResultVO<Role> roleSave(@RequestBody RoleRequestVO roleRequestVO) {

        LOGGER.debug("roleRequestVO :{}", roleRequestVO);
        ExeResultVO<Role> exeResult = null;
        // 表單驗證
        final Role role = this.roleService.getRoleByRoleId(StringUtils.lowerCase(roleRequestVO.getRoleId()));
        final List<MsgVO> msgs = RoleRequestValidator.validate(roleRequestVO, role);
        if (CollectionUtils.isNotEmpty(msgs)) {
            //表單格式錯誤
            exeResult = new ExeResultVO<>(FAILURE, StringUtils.EMPTY, null, msgs);
        } else {
            this.roleService.insertRole(roleRequestVO);
            exeResult = new ExeResultVO<>(SUCCESS, "角色新增成功", this.roleService.getAllRoles(), null);
        }

        return exeResult;
    }

    @ResponseBody
    @RequestMapping(value = "/api/adminRole/update", method = RequestMethod.PUT)
    public ExeResultVO<Role> roleUpdate(@RequestBody RoleRequestVO roleRequestVO) {

        LOGGER.debug("roleRequestVO :{}", roleRequestVO);
        ExeResultVO<Role> exeResult = null;
        final Role role = this.roleService.getRoleById(roleRequestVO.getId());
        final List<MsgVO> msgs = RoleRequestValidator.updateValidate(roleRequestVO, role);
        if (CollectionUtils.isNotEmpty(msgs)) {
            //表單格式錯誤
            exeResult = new ExeResultVO<>(FAILURE, StringUtils.EMPTY, null, msgs);
        } else {
            this.roleService.updateRole(roleRequestVO, role);
            exeResult = new ExeResultVO<>(SUCCESS, "角色更新成功", this.roleService.getAllRoles(), null);
        }

        return exeResult;

    }

    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
