
package com.wrel.admin.console.admin.controller;

import static com.wrel.admin.console.common.ExeCode.FAILURE;
import static com.wrel.admin.console.common.ExeCode.SUCCESS;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wrel.admin.console.common.ExeCode;
import com.wrel.admin.console.common.MsgVO;
import com.wrel.admin.console.common.OptionVO;
import com.wrel.admin.console.common.controller.BaseController;
import com.wrel.admin.console.page.ViewPage;
import com.wrel.admin.console.request.FunctionRequestVO;
import com.wrel.admin.console.response.ExeResultVO;
import com.wrel.admin.console.service.FunctionService;
import com.wrel.admin.console.validator.FunctionRequestValidator;
import com.wrel.admin.entity.Function;
import com.wrel.admin.entity.Function.Show;

/**
 *
 * Page/Class Name: FunctionController
 * Title:
 * Description:
 * Copyright: 
 * Company:	
 * author: 10503305 
 * Create Date:	2016年4月9日
 * Last Modify Date: 2016年4月9日
 * Version 1.0
 *
 */
@Controller
public class FunctionController extends BaseController {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    private final Logger LOGGER = LoggerFactory.getLogger(FunctionController.class);

    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    @Autowired
    private FunctionService functionService;

    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    /**
     * 
     * admin新增功能初始頁面
     * 
     * @return
     */
    @RequestMapping(value = "/adminFunction", method = RequestMethod.GET)
    public ModelAndView functionIndex() {

        final ModelAndView modelAndView = this.getModelAndView(ViewPage.ADMIN_FUNTION);
        List<Function> functions = this.functionService.getAllFunctions();
        final List<OptionVO> options = this.functionService.getFunctionSelects(functions);
        modelAndView.addObject("functions", functions);
        modelAndView.addObject("options", options);
        modelAndView.addObject("shows", Show.values());
        modelAndView.addObject("statuses", Function.Status.values());

        return modelAndView;

    }

    @ResponseBody
    @RequestMapping(value = "/api/adminFunction/add", method = RequestMethod.POST)
    public ExeResultVO<Function> functionSave(@RequestBody FunctionRequestVO functionRequestVO) {

        LOGGER.debug("functionRequestVO :{}", functionRequestVO);
        ExeResultVO<Function> exeResult = null;

        // 表單驗證        
        final List<MsgVO> msgs = FunctionRequestValidator.validate(functionRequestVO);
        if (CollectionUtils.isNotEmpty(msgs)) {
            //表單格式錯誤

            exeResult = new ExeResultVO<Function>(FAILURE, StringUtils.EMPTY, null, msgs);
        } else {

            this.functionService.insertFunction(functionRequestVO);

            List<Function> functions = this.functionService.getAllFunctions();

            exeResult = new ExeResultVO<Function>(SUCCESS, "功能新增成功", functions, null);
            final List<OptionVO> options = this.functionService.getFunctionSelects(functions);
            exeResult.setOptions(options);
        }

        return exeResult;
    }

    @ResponseBody
    @RequestMapping(value = "/api/adminFunction/get/{id}", method = RequestMethod.GET)
    public ExeResultVO<Function> getFunctionById(@PathVariable Long id) {

        LOGGER.debug("function id :{}", id);
        final Function function = this.functionService.getFunctionById(id);

        ExeResultVO<Function> exeResult = null;
        if (function == null) {
            exeResult = new ExeResultVO<Function>(ExeCode.FAILURE, null, "查詢失敗");
        } else {
            exeResult = new ExeResultVO<Function>(ExeCode.SUCCESS, function, StringUtils.EMPTY);
        }
        return exeResult;
    }

    @ResponseBody
    @RequestMapping(value = "/api/adminFunction/update", method = RequestMethod.PUT)
    public ExeResultVO<Function> functionUpdate(@RequestBody FunctionRequestVO functionRequestVO) {

        LOGGER.debug("functionRequestVO :{}", functionRequestVO);
        final Function function = this.functionService.getFunctionById(functionRequestVO.getId());
        ExeResultVO<Function> exeResult = null;
        final List<MsgVO> msgs = FunctionRequestValidator.updateValidate(functionRequestVO, function);
        if (CollectionUtils.isNotEmpty(msgs)) {
            //表單格式錯誤
            for (MsgVO msgVO : msgs) {
                System.out.println(msgVO.toString());
            }
            exeResult = new ExeResultVO<>(FAILURE, StringUtils.EMPTY, null, msgs);
        } else {

            this.functionService.updateFunction(functionRequestVO, function);
            List<Function> functions = this.functionService.getAllFunctions();
            final List<OptionVO> options = this.functionService.getFunctionSelects(functions);
            exeResult = new ExeResultVO<>(SUCCESS, "功能成功", functions, null);
            exeResult.setOptions(options);
        }

        return exeResult;

    }

    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
