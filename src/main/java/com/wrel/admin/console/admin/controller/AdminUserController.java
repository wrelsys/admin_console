
package com.wrel.admin.console.admin.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wrel.admin.console.common.ExeCode;
import com.wrel.admin.console.common.controller.BaseController;
import com.wrel.admin.console.page.ViewPage;
import com.wrel.admin.console.request.UserQueryRequestVO;
import com.wrel.admin.console.response.ExeResultVO;
import com.wrel.admin.console.service.UserService;
import com.wrel.admin.entity.User;

/**
 *
 * Page/Class Name: AdminUserController
 * Title:
 * Description:
 * Copyright:
 * Company:
 * author: weiting 
 * Create Date:	2016年4月17日
 * Last Modifier: weiting
 * Last Modify Date: 2016年4月17日
 * Version 1.0
 *
 */
@Controller
public class AdminUserController extends BaseController {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    private final Logger LOGGER = LoggerFactory.getLogger(AdminUserController.class);

    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    @Autowired
    private UserService userService;

    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    @RequestMapping(value = "/adminUser", method = RequestMethod.GET)
    public ModelAndView userIndex() {

        final ModelAndView modelAndView = this.getModelAndView(ViewPage.ADMIN_USER);
        modelAndView.addObject("statuses", User.Status.values());
        return modelAndView;

    }

    @ResponseBody
    @RequestMapping(value = "/api/adminUser/get/{id}", method = RequestMethod.GET)
    public ExeResultVO<User> getUserById(@PathVariable Long id) {

        LOGGER.debug("user id :{}", id);
        final User user = this.userService.getUserById(id);
        ExeResultVO<User> exeResult = null;
        if (user == null) {
            exeResult = new ExeResultVO<User>(ExeCode.FAILURE, null, "查詢失敗");
        } else {
            exeResult = new ExeResultVO<User>(ExeCode.SUCCESS, user, StringUtils.EMPTY);
        }
        return exeResult;
    }

    @ResponseBody
    @RequestMapping(value = "/api/adminUser/query", method = RequestMethod.POST)
    public ExeResultVO<User> userQuery(@RequestBody UserQueryRequestVO requestVO) {
        LOGGER.debug("requestVO :{}", requestVO.toString());
        ExeResultVO<User> result =
            new ExeResultVO<User>(ExeCode.SUCCESS, StringUtils.EMPTY, this.userService.queryUser(requestVO), null);

        LOGGER.debug(result.toString());
        return result;
    }
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
