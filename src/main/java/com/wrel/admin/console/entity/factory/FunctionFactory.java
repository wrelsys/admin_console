
package com.wrel.admin.console.entity.factory;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.wrel.admin.console.request.FunctionRequestVO;
import com.wrel.admin.entity.Function;
import com.wrel.admin.entity.Function.Show;

/**
 *
 * Page/Class Name: FunctionFactory
 * Title:
 * Description:
 * Copyright: 
 * Company:	
 * author: 10503305 
 * Create Date:	2016年4月10日
 * Last Modify Date: 2016年4月10日
 * Version 1.0
 *
 */
public class FunctionFactory {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====

    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    public static Function getNewFunctionInstance(final FunctionRequestVO requestVO) {

        final Function function = new Function(//
                requestVO.getFunctionName(), //
                requestVO.getFunctionUrl(), //
                Show.lookup(requestVO.getShow()).isValue(), //
                requestVO.getStatus(), //
                StringUtils.isBlank(requestVO.getOrder()) ? null : Integer.valueOf(requestVO.getOrder()), //
                null);

        final Date now = new Date();
        function.setCreateDate(now);
        function.setModifyDate(now);
        return function;
    }
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
