
package com.wrel.admin.console.entity.factory;

import static com.wrel.admin.entity.User.Status.NON_VERIFY;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.wrel.admin.console.request.RegisterRequestVO;
import com.wrel.admin.console.request.UserQueryRequestVO;
import com.wrel.admin.entity.User;
import com.wrel.common.utils.DateUtils;
import com.wrel.common.utils.DateUtils.DateFormat;
import com.wrel.common.utils.SecurityUtils;

/**
 *
 * Page/Class Name: UserFactory
 * Title:
 * Description:
 * Copyright: 
 * Company:	President Information Corp.
 * author: Weiting
 * Create Date:	2016年4月4日 
 * Last Modify Date: 2016年4月4日
 * Version 1.0
 *
 */
public class UserFactory {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    public static User getNewUserInstance(final RegisterRequestVO registerRequestVO) {
        final User user = new User(//
                registerRequestVO.getName(), //
                SecurityUtils.encodeWithMD5AndBase64(registerRequestVO.getPassword()), //
                registerRequestVO.getEmail(), //
                registerRequestVO.getTel(), //
                registerRequestVO.getMobile(), //
                NON_VERIFY);
        final Date now = new Date();
        user.setCreateTime(now);
        user.setModifyTime(now);
        return user;
    }

    public static User getQueryUserInstance(final UserQueryRequestVO userQueryRequestVO) {
        final User user = new User();
        if (StringUtils.isNotBlank(userQueryRequestVO.getEmail())) {
            user.setEmail("%" + userQueryRequestVO.getEmail() + "%");
        }

        if (StringUtils.isNotBlank(userQueryRequestVO.getStatus())) {
            user.setStatus(Integer.valueOf(userQueryRequestVO.getStatus()));
        }

        final String dateRanges[] = StringUtils.split(userQueryRequestVO.getDateRange(), "-");
        int index = 0;
        for (String str : dateRanges) {
            
            if (index == 0) {
                user.setStartCreateDate(DateUtils.strToDate(str.trim(), DateFormat.MONTH_DAY_YEAR));
            } else if (index == 1) {
                user.setEndCreateDate(DateUtils.strToDate(str.trim(), DateFormat.MONTH_DAY_YEAR));
            }
            index++;
        }

        return user;
    }
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
