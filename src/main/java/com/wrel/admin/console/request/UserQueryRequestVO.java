
package com.wrel.admin.console.request;

import java.io.Serializable;

import com.wrel.admin.console.common.BasicVO;

/**
 *
 * Page/Class Name: UserQueryRequestVO
 * Title:
 * Description:
 * Copyright:
 * Company:
 * author: weiting 
 * Create Date:	2016年4月17日
 * Last Modifier: weiting
 * Last Modify Date: 2016年4月17日
 * Version 1.0
 *
 */
public class UserQueryRequestVO extends BasicVO implements Serializable {

    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    /**
     * <code>serialVersionUID</code> 的註解
     */
    private static final long serialVersionUID = -3584116508049471546L;

    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
    private String email;

    private String status;

    private String dateRange;
    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block StartÍ
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================

    public String getEmail() {
        return this.email;
    }

    public String getDateRange() {
        return this.dateRange;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDateRange(String dateRange) {
        this.dateRange = dateRange;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
