
package com.wrel.admin.console.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@ComponentScan({ "com.wrel.admin.dao" })
@PropertySource(name = "props", value = { "classpath:/application.properties", "classpath:/wrel.properties" })
public class SpringRootConfig {

    // ================================================
    // == [Enumeration types] Block Start
    // ====
    // ====
    // == [Enumeration types] Block End
    // ================================================
    // == [static variables] Block Start
    // ====
    // ====
    // == [static variables] Block Stop
    // ================================================
    // == [instance variables] Block Start
    // ====
    // ====
    // == [instance variables] Block Stop
    // ================================================
    // == [static Constructor] Block Start
    // ====
    // ====
    // == [static Constructor] Block Stop
    // ================================================
    // == [Constructors] Block Start (含init method)
    // ====
    // ====
    // == [Constructors] Block Stop
    // ================================================
    // == [Static Method] Block Start
    // ====
    // ====
    // == [Static Method] Block Stop
    // ================================================
    // == [Accessor] Block Start
    // ====
    // ====
    // == [Accessor] Block Stop
    // ================================================
    // == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    // ====
    // ====
    // == [Overrided Method] Block Stop
    // ================================================
    // == [Method] Block Start
    // ====
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
    // ####################################################################
    // ## [Method] sub-block :
    // ####################################################################
    // ====
    // == [Method] Block Stop
    // ================================================
}
