<%@ page contentType="text/html; charset=utf-8"%>
<style>
.example-modal .modal {
	position: relative;
	top: auto;
	bottom: auto;
	right: auto;
	left: auto;
	display: block;
	z-index: 1;
}

.example-modal .modal {
	background: transparent !important;
}
</style>

<div class="modal fade" tabindex="-1" role="dialog" id="danger_modal">
	<div class="example-modal" tabindex="-1" role="dialog">
		<div class="modal modal-danger">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">訊息通知</h4>
					</div>
					<div class="modal-body" id="danger_body">
						<p></p>
					</div>
					<div class="modal-footer">

						<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
	</div>
	<!-- /.example-modal -->
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="success_modal">
	<div class="example-modal" tabindex="-1" role="dialog">
		<div class="example-modal">
			<div class="modal modal-success">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title">訊息通知</h4>
						</div>
						<div class="modal-body" id="success_body">
							<p></p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-outline"
								data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
		</div>
		<!-- /.example-modal -->
	</div>
</div>

