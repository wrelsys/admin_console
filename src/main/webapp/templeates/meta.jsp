<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>WREL Admin</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet"
	href="<c:url value="resources/plugins/bootstrap/css/bootstrap.min.css"/>">

<!-- Bootstrap time Picker -->
<link rel="stylesheet"
	href="<c:url value="resources/plugins/timepicker/bootstrap-timepicker.min.css"/>">

<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- daterange picker -->
<link rel="stylesheet"
	href="<c:url value="resources/plugins/daterangepicker/daterangepicker-bs3.css"/>">
<!-- Theme style -->
<link rel="stylesheet"
	href="<c:url value="resources/css/AdminLTE.min.css"/>">
<!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="<c:url value="resources/css/skins/_all-skins.min.css"/>">
<!-- iCheck -->
<link rel="stylesheet"
	href="<c:url value="resources/plugins/iCheck/flat/blue.css"/>">
<link rel="stylesheet"
	href="<c:url value="resources/plugins/iCheck/all.css"/>">
<link rel="stylesheet"
	href="<c:url value="resources/plugins/select2/select2.min.css"/>">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet"
	href="<c:url value="resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"/>">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->