<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- jQuery 2.1.4 -->
<script src="<c:url value="resources/js/jQuery-2.1.4.min.js"/>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script
	src="<c:url value="resources/plugins/bootstrap/js/bootstrap.min.js"/>"></script>
<!-- Morris.js charts -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<!-- Sparkline -->
<script
	src="<c:url value="resources/plugins/sparkline/jquery.sparkline.min.js"/>"></script>
<!-- jvectormap -->
<script
	src="<c:url value="resources/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"/>"></script>
<script
	src="<c:url value="resources/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"/>"></script>
<!-- jQuery Knob Chart -->
<script src="<c:url value="resources/plugins/knob/jquery.knob.js"/>"></script>
<!-- daterangepicker -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script
	src="<c:url value="resources/plugins/daterangepicker/daterangepicker.js"/>"></script>
<!-- datepicker -->
<script
	src="<c:url value="resources/plugins/datepicker/bootstrap-datepicker.js"/>"></script>

<!-- bootstrap time picker -->
<script
	src="<c:url value="resources/plugins/timepicker/bootstrap-timepicker.min.js"/>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script
	src="<c:url value="resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"/>"></script>
<!-- Slimscroll -->
<script
	src="<c:url value="resources/plugins/slimScroll/jquery.slimscroll.min.js"/>"></script>
<!-- FastClick -->
<script
	src="<c:url value="resources/plugins/fastclick/fastclick.min.js"/>"></script>
<!-- AdminLTE App -->
<script src="<c:url value="resources/js/app.min.js"/>"></script>

<!-- Select2 -->
<script
	src="<c:url value="resources/plugins/select2/select2.full.min.js"/>"></script>
<script src="<c:url value="resources/plugins/iCheck/icheck.min.js"/>"></script>
<script src="<c:url value="resources/js/wrel.js"/>"></script>
<script>
	$(function() {

		//Initialize Select2 Elements
		$(".select2").select2();

		//iCheck for checkbox and radio inputs
		$('input[type="checkbox"].minimal, input[type="radio"].minimal')
				.iCheck({
					checkboxClass : 'icheckbox_minimal-blue',
					radioClass : 'iradio_minimal-blue'
				});
		//Red color scheme for iCheck
		$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red')
				.iCheck({
					checkboxClass : 'icheckbox_minimal-red',
					radioClass : 'iradio_minimal-red'
				});
		//Flat red color scheme for iCheck
		$('input[type="checkbox"].flat-red, input[type="radio"].flat-red')
				.iCheck({
					checkboxClass : 'icheckbox_flat-green',
					radioClass : 'iradio_flat-green'
				});
		var msg = '${msg}';
		if (msg != null && msg != '') {
			modalShow('${type}', msg);
		}

		//Timepicker
		$(".timepicker").timepicker({
			showInputs : false
		});

	});
</script>

<jsp:include page="/templeates/modal.jsp" />
