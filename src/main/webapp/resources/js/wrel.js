function modalShow(type, msg) {

	if (type == "danger") {
		$('#danger_body').empty();
		$('#danger_body').html(msg);
		$('#danger_modal').modal('show');
	} else if (type == "success") {
		$('#success_body').empty();
		$('#success_body').html(msg);
		$('#success_modal').modal('show');
	}
}

function datePicker(id) {	
	$('#'+id).daterangepicker();
	
}
//

function cleanInputValueById(id) {

	$("input[id^='" + id + "']").val('');
	$("div[id^='msg_" + id + "']").empty();
}

function radioEnable(form) {
	$('#' + form + ' input[type="radio"]').removeAttr("disabled");
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
		checkboxClass : 'icheckbox_minimal-blue',
		radioClass : 'iradio_minimal-blue'
	});
	// Red color scheme for iCheck
	$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red')
			.iCheck({
				checkboxClass : 'icheckbox_minimal-red',
				radioClass : 'iradio_minimal-red'
			});
	// Flat red color scheme for iCheck
	$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
		checkboxClass : 'icheckbox_flat-green',
		radioClass : 'iradio_flat-green'
	});

}