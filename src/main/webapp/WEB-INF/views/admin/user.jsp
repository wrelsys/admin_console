<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">

<head>
<jsp:include page="/templeates/meta.jsp" />
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<jsp:include page="/templeates/header.jsp" />
		<!-- Left side column. contains the logo and sidebar -->


		<jsp:include page="/templeates/leftMenu.jsp" />

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<section class="content-header">
				<h1>
					General Form Elements <small>Preview</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Forms</a></li>
					<li class="active">General Elements</li>
				</ol>
			</section>
			<section class="content">
				<div class="row">
					<!-- left column  start-->

					<!-- left column  start-->
					<!-- right column -->
					<div class="col-md-6">

						<!-- Horizontal Form -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">查詢使用者</h3>
							</div>
							<!-- /.box-header -->
							<!-- form start -->
							<form id="query_userForm" class="form-horizontal">
								<div class="box-body">
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
										<div class="col-sm-10">
											<input type="text" id="query_email" class="form-control"
												placeholder="xxx.xxx@gmail.com" />
											<div id="msg_query_email"></div>
										</div>
									</div>
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-2 control-label">使用者狀態</label>
										<div class="col-sm-10">
											<input type="radio" name="query_userStatus" checked="checked"
												value="" class=" flat-red" />全部&nbsp;&nbsp;
											<c:forEach var="status" items="${statuses}" varStatus="sn">
												<input type="radio" name="query_userStatus"
													value="${status.value}" class=" flat-red" />${status.title}&nbsp;&nbsp;																																			
											</c:forEach>
											<div id="msg_query_userStatus"></div>
										</div>
									</div>

									<!-- Date range -->
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-2 control-label">建立日期</label>
										<div class="col-sm-10">
											<div class="input-group">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" class="form-control pull-right"
													id="query_createDate" readonly="readonly">
											</div>
										</div>
										<!-- /.input group -->
									</div>
									<!-- /.form group -->


								</div>
								<!-- /.box-body -->
								<div class="box-footer">
									<button type="button" onclick="queryUserSend();"
										class="btn btn-info pull-right">查詢</button>
								</div>
								<!-- /.box-footer -->

							</form>
						</div>
						<!-- /.box -->
					</div>

					<!-- /.box -->
					<div class="col-md-6">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Condensed Full Width Table</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-condensed" id="user_table">
									<tr>
										<th style="width: 10px">#</th>
										<th>USWER NAME</th>
										<th>EMAIL</th>
										<th>STATUS</th>
										<th style="width: 40px">ACTION</th>
									</tr>

								</table>
							</div>
							<!-- /.box-body -->
						</div>


					</div>
				</div>
			</section>
		</div>

		<!-- /.content-wrapper -->
		<jsp:include page="/templeates/footer.jsp" />

		<jsp:include page="/templeates/metaImport.jsp" />
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>


	<!--  edit role form start-->

	<div class="modal fade" tabindex="-1" role="dialog" id="edit_userModal">
		<div class="example-modal" tabindex="-1" role="dialog">
			<div class="modal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title">編輯使用者</h4>
						</div>
						<div class="modal-body">
							<div class="box box-info">
								<!-- form start -->
								<form id="edit_userForm" class="form-horizontal">
									<input type="hidden" id="edit_id" value="" />
									<div class="box-body">
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-2 control-label">使用者名稱</label>
											<div class="col-sm-10">
												<input type="text" id="edit_name" class="form-control"
													disabled="disabled" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">Email</label>
											<div class="col-sm-10">
												<input type="text" id="edit_email" class="form-control"
													disabled="disabled" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">手機</label>
											<div class="col-sm-10">
												<input type="text" id="edit_mobile" class="form-control" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">電話</label>
											<div class="col-sm-10">
												<input type="text" id="edit_tel" class="form-control" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">使用者狀態</label>
											<div class="col-sm-10">
												<c:forEach var="status" items="${statuses}" varStatus="sn">

													<input type="radio" name="edit_userStatus"
														value="${status.value}" class=" flat-red" />${status.title}
																														
											</c:forEach>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left"
								data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary"
								onclick="editRoleSend();">Save changes</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
		</div>
		<!-- /.example-modal -->
	</div>


	<!--  edit role form end -->



	<!-- ./wrapper -->
	<script type="text/javascript">
		function getUserToEdit(id) {
			var prefix = 'edit_';
			var radioValue;
			$.ajax({
				url : '<c:url value="api/adminUser/get/'+id+'"/>',
				type : 'get',
				contentType : "application/json; charset=utf-8",
				success : function(result) {
					if (result.exeCode == 1) {
						$("#" + prefix + 'id').val(result.obj.id);
						$("#" + prefix + 'name').val(result.obj.name);
						$("#" + prefix + 'email').val(result.obj.email);
						$("#" + prefix + 'mobile').val(result.obj.mobile);
						$("#" + prefix + 'tel').val(result.obj.tel);						
						var radioValue = $(
								'input[name=edit_userStatus]:checked',
								'#edit_userForm').val();
						if (radioValue != result.obj.status) {
							$("input[name=edit_userStatus]").prop('checked',
									false);
							$(
									"input[name=edit_userStatus][value='"
											+ result.obj.status + "']").prop(
									'checked', true);
							radioEnable('edit_userForm');
						}

						$('#edit_userModal').modal('show');
					} else if (result.exeCode == 0) {
						alert(result.msg);
					}
				}

			});
		}
		function editRoleSend() {
			var prefix = 'edit_';
			var editRole = {
				id : $("#" + prefix + "id").val(),
				roleName : $("#" + prefix + "roleName").val(),
				status : $(
						"#" + prefix + "roleForm input[type='radio']:checked")
						.val()
			}

			$.ajax({
				url : '<c:url value="api/adminRole/update"/>',
				type : 'put',
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(editRole),
				success : function(result) {
					if (result.exeCode == 1) {
						cleanInputValueById(prefix);
						$("tr[id^='user_list_']").remove();

						$.each(result.datas, function(index, role) {
							appendTable('user_table', index, role);
						});
						$('#edit_RoleModal').modal('hide');
						modalShow('success', result.successMsg);
					} else if (result.exeCode == 0) {
						$("div[id^='msg_edit_']").empty();
						$.each(result.errorMsgs, function(index, msg) {
							$("#msg_edit_" + msg.code).html(
									'<font color=\"red\">' + msg.value
											+ '</font>');
						});
					}
				}

			});
		}

		function queryUserSend() {
			var prefix = 'query_';
			var queryVO = {
				email : $("#" + prefix + "email").val(),
				status : $(
						"#" + prefix + "userForm input[type='radio']:checked")
						.val(),
				dateRange : $("#" + prefix + "createDate").val()
			}

			$.ajax({
				url : '<c:url value="api/adminUser/query"/>',
				type : 'post',
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(queryVO),
				success : function(result) {
					if (result.exeCode == 1) {
						$("tr[id^='user_list_']").remove();

						$.each(result.datas, function(index, user) {
							appendTable('user_table', index, user);
						});
					}
				}

			});
		}
		function appendTable(id, index, user) {
			var tr = '';
			tr = tr + '<tr id=\"user_list_'+index+'\">';
			tr = tr + '<td>' + (index + 1) + '</td>';
			tr = tr + '<td>' + user.name + '</td>';
			tr = tr + '<td>' + user.email + '</td>';
			tr = tr + '<td>' + user.showStatus + '</td>';
			tr = tr
					+ '<td><button class=\"btn btn-block btn-sm\" onclick=\"getUserToEdit('
					+ user.id + ');\" >編輯</button></td>';
			tr = tr + '</tr>';
			$('#' + id + ' tr:last').after(tr);

		}

		$(function() {
			$('#query_createDate').daterangepicker();
		});
	</script>

</body>

</html>
