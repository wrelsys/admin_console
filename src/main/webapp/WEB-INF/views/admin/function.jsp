<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">

<head>
<jsp:include page="/templeates/meta.jsp" />
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<jsp:include page="/templeates/header.jsp" />
		<!-- Left side column. contains the logo and sidebar -->


		<jsp:include page="/templeates/leftMenu.jsp" />

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<section class="content-header">
				<h1>
					General Form Elements <small>Preview</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Forms</a></li>
					<li class="active">General Elements</li>
				</ol>
			</section>
			<section class="content">
				<div class="row">
					<!-- left column  start-->
					<div class="col-md-6">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Condensed Full Width Table</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-condensed" id="function_table">
									<tr>
										<th style="width: 10px">#</th>
										<th>FUNCTION NAME</th>
										<th>FUNCTION URL</th>
										<th>PARENT</th>
										<th>STATUS</th>
										<th>ORDER</th>
										<th style="width: 40px">Action</th>
									</tr>
									<c:forEach items="${functions}" var="function" varStatus="sn">
										<tr id="function_list_${sn.index}">
											<td><b>${sn.index+1}.</b></td>
											<td><b>${function.functionName}</b></td>
											<td><b>${function.functionUrl}</b></td>
											<c:choose>
												<c:when test="${function.parent != null}">
													<td><b>${function.parent.id}</b></td>
												</c:when>
												<c:otherwise>
													<td>&nbsp;&nbsp;</td>
												</c:otherwise>
											</c:choose>
											<td><b>${function.showStatus}</b></td>
											<td><b>${function.order}</b></td>
											<td><button class="btn btn-block btn-sm"
													onclick="getFunctionToEdit('${function.id}');">編輯</button></td>
										</tr>
										<c:if test="${function.subFunctions != null}">
											<c:forEach items="${function.subFunctions}" var="subFunction"
												varStatus="sn">
												<tr id="function_list_s_${sn.index}">
													<td>&nbsp;&nbsp;</td>
													<td>${subFunction.functionName}</td>
													<td>${subFunction.functionUrl}</td>
													<td>${subFunction.parent.id}</td>
													<td>${subFunction.showStatus}</td>
													<td>${subFunction.order}</td>
													<td><button class="btn btn-block btn-sm"
															onclick="getFunctionToEdit('${subFunction.id}');">編輯</button></td>
												</tr>
											</c:forEach>
										</c:if>

									</c:forEach>
								</table>
							</div>
							<!-- /.box-body -->
						</div>


					</div>
					<!-- left column  start-->
					<!-- right column -->
					<div class="col-md-6">

						<!-- Horizontal Form -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">新增功能</h3>
							</div>
							<!-- /.box-header -->
							<!-- form start -->
							<form id="add_functionForm" class="form-horizontal">
								<div class="box-body">
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-2 control-label">功能名稱</label>
										<div class="col-sm-10">
											<input type="text" id="add_functionName" class="form-control"
												placeholder="使用者管理" />
											<div id="msg_add_functionName"></div>
										</div>
									</div>
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-2 control-label">功能URL</label>
										<div class="col-sm-10">
											<input type="text" id="add_functionUrl" class="form-control"
												placeholder="功能連結 ex：/addFunction" />
											<div id="msg_add_functionUrl"></div>
										</div>
									</div>
									<div class="form-group">

										<label for="inputPassword3" class="col-sm-2 control-label">Parent
											Function</label>
										<div class="col-sm-10">
											<select id="add_parentId" class="form-control select2"
												style="width: 100%;">
												<option selected="selected" value="">下拉選擇</option>
												<c:forEach items="${options}" var="option">
													<option value="${option.value}">${option.label}</option>
												</c:forEach>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-2 control-label">功能狀態</label>
										<div class="col-sm-10">
											<c:forEach var="status" items="${statuses}" varStatus="sn">
												<c:choose>
													<c:when test="${sn.index == 0 }">
														<input type="radio" name="add_functionStatus"
															checked="checked" value="${status.value}"
															class=" flat-red" />${status.title}
													</c:when>
													<c:otherwise>
														<input type="radio" name="add_functionStatus"
															value="${status.value}" class=" flat-red" />${status.title}
											</c:otherwise>
												</c:choose>

											</c:forEach>
											<div id="msg_add_functionStatus"></div>
										</div>
									</div>
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-2 control-label">是否呈現</label>
										<div class="col-sm-10">
											<c:forEach var="show" items="${shows}" varStatus="sn">
												<c:choose>
													<c:when test="${sn.index == 0 }">
														<input type="radio" name="add_show" checked="checked"
															value="${show}" class=" flat-red" />${show}
													</c:when>
													<c:otherwise>
														<input type="radio" name="add_show" value="${show}"
															class=" flat-red" />${show}
											</c:otherwise>
												</c:choose>

											</c:forEach>
											<div id="msg_add_show"></div>
										</div>
									</div>
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-2 control-label">功能排序</label>
										<div class="col-sm-10">
											<input type="text" id="add_functionOrder"
												class="form-control" maxlength="2" placeholder="功能排序  ex：1" />
											<div id="msg_add_order"></div>
										</div>
									</div>
								</div>
								<!-- /.box-body -->
								<div class="box-footer">
									<button type="button" onclick="addFunctionSend();"
										class="btn btn-info pull-right">新增功能</button>
								</div>
								<!-- /.box-footer -->

							</form>
						</div>
						<!-- /.box -->
					</div>

					<!-- /.box -->
				</div>
			</section>
		</div>

		<!-- /.content-wrapper -->
		<jsp:include page="/templeates/footer.jsp" />

		<jsp:include page="/templeates/metaImport.jsp" />
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>


	<!--  edit role form start-->

	<div class="modal fade" tabindex="-1" role="dialog" id="edit_FunctionModal">
		<div class="example-modal" tabindex="-1" role="dialog">
			<div class="modal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title">編輯功能</h4>
						</div>
						<div class="modal-body">
							<div class="box box-info">
								<!-- form start -->
								<form id="edit_functionForm" class="form-horizontal">
									<input type="hidden" id="edit_id" value="" />
									<div class="box-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">功能名稱</label>
											<div class="col-sm-10">
												<input type="text" id="edit_functionName"
													class="form-control" placeholder="使用者管理" />
												<div id="msg_edit_functionName"></div>
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">功能URL</label>
											<div class="col-sm-10">
												<input type="text" id="edit_functionUrl" class="form-control"
													placeholder="功能連結 ex：/addFunction" />
												<div id="msg_edit_functionUrl"></div>
											</div>
										</div>
										<div class="form-group">

											<label for="inputPassword3" class="col-sm-2 control-label">Parent
												Function</label>
											<div class="col-sm-10">
												<select id="edit_parentId" class="form-control select2"
													style="width: 100%;">
													<option selected="selected" value="">下拉選擇</option>
													<c:forEach items="${options}" var="option">
														<option value="${option.value}">${option.label}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">功能狀態</label>
											<div class="col-sm-10">
												<c:forEach var="status" items="${statuses}" varStatus="sn">
													<c:choose>
														<c:when test="${sn.index == 0 }">
															<input type="radio" name="edit_functionStatus"
																checked="checked" value="${status.value}"
																class=" flat-red" />${status.title}
													</c:when>
														<c:otherwise>
															<input type="radio" name="edit_functionStatus"
																value="${status.value}" class=" flat-red" />${status.title}
											</c:otherwise>
													</c:choose>

												</c:forEach>
												<div id="msg_edit_functionStatus"></div>
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">是否呈現</label>
											<div class="col-sm-10">
												<c:forEach var="show" items="${shows}" varStatus="sn">
													<c:choose>
														<c:when test="${sn.index == 0 }">
															<input type="radio" name="edit_show" checked="checked"
																value="${show}" class=" flat-red" />${show}
													</c:when>
														<c:otherwise>
															<input type="radio" name="edit_show" value="${show}"
																class=" flat-red" />${show}
											</c:otherwise>
													</c:choose>

												</c:forEach>
												<div id="msg_edit_show"></div>
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">功能排序</label>
											<div class="col-sm-10">
												<input type="text" id="edit_functionOrder"
													class="form-control" maxlength="2" placeholder="功能排序  ex：1" />
												<div id="msg_edit_order"></div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left"
								data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary"
								onclick="editFunctionSend();">Save changes</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
		</div>
		<!-- /.example-modal -->
	</div>


	<!--  edit role form end -->



	<!-- ./wrapper -->
	<script type="text/javascript">
		function getFunctionToEdit(id) {
			var prefix = 'edit_';
			var radioValue;
			$.ajax({
				url : '<c:url value="api/adminFunction/get/'+id+'"/>',
				type : 'get',
				contentType : "application/json; charset=utf-8",
				success : function(result) {
					if (result.exeCode == 1) {
																			
						$("#" + prefix + 'id').val(result.obj.id);
						$("#" + prefix + 'functionName').val(result.obj.functionName);
						$("#" + prefix + 'functionUrl').val(result.obj.functionUrl);
						$("#" + prefix + 'functionOrder').val(result.obj.order);
						
						if(result.obj.parentId != null){
						 $("#" + prefix + "parentId").val(result.obj.parentId);
						}
						else{
							$("#" + prefix + "parentId").val('');
						}
						var statusRadioValue = $(
								'input[name=edit_functionStatus]:checked',
								'#edit_functionForm').val();
						if (statusRadioValue != result.obj.status) {
							$("input[name=edit_functionStatus]").prop('checked',
									false);
							$("input[name=edit_functionStatus][value='"
											+ result.obj.status + "']").prop(
									'checked', true);							
						}
						var showRadioValue = $(
								'input[name=edit_show]:checked',
								'#edit_functionForm').val();
						if (showRadioValue != result.obj.showStr) {
							$("input[name=edit_show]").prop('checked',
									false);
							$("input[name=edit_show][value='"
											+ result.obj.showStr + "']").prop(
									'checked', true);						
						}						
						radioEnable('edit_functionForm');
						$('#edit_FunctionModal').modal('show');
					} else if (result.exeCode == 0) {
						alert(result.msg);
					}
				}

			});
		}
		function editFunctionSend() {
			var prefix = 'edit_';
			var editFunction = {
					id : $("#" + prefix + "id").val(),
					functionName : $("#" + prefix + "functionName").val(),
					functionUrl : $("#" + prefix + "functionUrl").val(),
					parentId : $("#" + prefix + "parentId").val(),
					order : $("#" + prefix + "functionOrder").val(),
					status : $(
							"#"
									+ prefix
									+ "functionForm input[name^='edit_functionStatus']:checked")
							.val(),
					show : $(
							"#"
									+ prefix
									+ "functionForm input[name^='edit_show']:checked")
							.val()
				}

			$.ajax({
				url : '<c:url value="api/adminFunction/update"/>',
				type : 'put',
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(editFunction),
				success : function(result) {
					if (result.exeCode == 1) {
						cleanInputValueById(prefix);
						$("tr[id^='function_list_']").remove();

						$.each(result.datas, function(index, wrelFunction) {
							appendTable('function_table', index, wrelFunction,
									true);
							$.each(wrelFunction.subFunctions, function(i,
									subFunction) {
								appendTable('function_table', index,
										subFunction, false);
							});
						});
						refreshSelect(prefix+"parentId", result.options);			
						$('#edit_FunctionModal').modal('hide');
						modalShow('success', result.successMsg);
					} else if (result.exeCode == 0) {
						$("div[id^='msg_edit_']").empty();
						$.each(result.errorMsgs, function(index, msg) {
							$("#msg_edit_" + msg.code).html(
									'<font color=\"red\">' + msg.value
											+ '</font>');
						});
					}
				}

			});
		}

		function addFunctionSend() {
			var prefix = 'add_';

			var newFunction = {
				functionName : $("#" + prefix + "functionName").val(),
				functionUrl : $("#" + prefix + "functionUrl").val(),
				parentId : $("#" + prefix + "parentId").val(),
				order : $("#" + prefix + "functionOrder").val(),
				status : $(
						"#"
								+ prefix
								+ "functionForm input[name^='add_functionStatus']:checked")
						.val(),
				show : $(
						"#"
								+ prefix
								+ "functionForm input[name^='add_show']:checked")
						.val()
			}

			$.ajax({
				url : '<c:url value="api/adminFunction/add"/>',
				type : 'post',
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(newFunction),
				success : function(result) {
					if (result.exeCode == 1) {
						cleanInputValueById(prefix);
						$("tr[id^='function_list_']").remove();

						$.each(result.datas, function(index, wrelFunction) {
							appendTable('function_table', index, wrelFunction,
									true);
							$.each(wrelFunction.subFunctions, function(i,
									subFunction) {
								appendTable('function_table', index,
										subFunction, false);
							});
						});
						refreshSelect(prefix+"parentId", result.options);						

						$('#add_parentId').val('');
						$("input[name=add_functionStatus][value='0']").prop(
								'checked', true);
						$("input[name=add_show][value='YES']").prop('checked',
								true);
						radioEnable(prefix + "functionForm");
						modalShow('success', result.successMsg);
					} else if (result.exeCode == 0) {
						$("div[id^='msg_" + prefix + "']").empty();
						$.each(result.errorMsgs, function(index, msg) {
							$("#msg_add_" + msg.code).html(
									'<font color=\"red\">' + msg.value
											+ '</font>');
						});
					}
				}

			});
		}
		function refreshSelect(id, options){
			$('#'+id).find('option').remove();
			$('#'+id).append(
					'<option value="">請下拉選擇</option>')
			$.each(options, function(x, option) {
				$('#'+id).append(
						'<option value="'+option.value+'">'
								+ option.label + '</option>')
			});

		}
		function appendTable(id, index, wrelFunction, parentFunction) {
			var tr = '';
			if (parentFunction) {
				tr = tr + '<tr id=\"function_list_'+index+'\">';
				tr = tr + '<td>' + (index + 1) + '</td>';
				tr = tr + '<td><b>' + wrelFunction.functionName + '</b></td>';
				tr = tr + '<td><b>' + wrelFunction.functionUrl + '</b></td>';
			} else {
				tr = tr + '<tr id=\"function_list_s'+index+'\">';
				tr = tr + '<td>&nbsp;&nbsp;</td>';
				tr = tr + '<td>' + wrelFunction.functionName + '</td>';
				tr = tr + '<td>' + wrelFunction.functionUrl + '</td>';
			}

			if (wrelFunction.parent != null) {
				tr = tr + '<td>' + wrelFunction.parent.functionName + '</td>';
			} else {
				tr = tr + '<td>&nbsp;&nbsp;</td>';
			}
			if (parentFunction) {
				tr = tr + '<td><b>' + wrelFunction.showStatus + '</b></td>';
			} else {
				tr = tr + '<td>' + wrelFunction.showStatus + '</td>';
			}
			if (wrelFunction.order != null) {
				tr = tr + '<td>' + wrelFunction.order + '</td>';
			} else {
				tr = tr + '<td>&nbsp;&nbsp;</td>';
			}
			tr = tr
					+ '<td><button class=\"btn btn-block btn-sm\" onclick=\"getFunctionToEdit('
					+ wrelFunction.id + ');\" >編輯</button></td>';
			tr = tr + '</tr>';
			$('#' + id + ' tr:last').after(tr);

		}
	</script>

</body>

</html>
