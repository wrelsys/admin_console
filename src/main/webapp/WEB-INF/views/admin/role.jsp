<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">

<head>
<jsp:include page="/templeates/meta.jsp" />
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<jsp:include page="/templeates/header.jsp" />
		<!-- Left side column. contains the logo and sidebar -->


		<jsp:include page="/templeates/leftMenu.jsp" />

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<section class="content-header">
				<h1>
					General Form Elements <small>Preview</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Forms</a></li>
					<li class="active">General Elements</li>
				</ol>
			</section>
			<section class="content">
				<div class="row">
					<!-- left column  start-->
					<div class="col-md-6">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Condensed Full Width Table</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-condensed" id="role_table">
									<tr>
										<th style="width: 10px">#</th>
										<th>ROLE ID</th>
										<th>ROLE NAME</th>
										<th>STATUS</th>
										<th style="width: 40px">Action</th>
									</tr>
									<c:forEach items="${roles}" var="role" varStatus="sn">
										<tr id="role_list_${sn.index}">
											<td>${sn.index+1}.</td>
											<td>${role.roleId}</td>
											<td>${role.roleName}</td>
											<td>${role.showStatus}</td>
											<td><button class="btn btn-block btn-sm"
													onclick="getRoleToEdit('${role.id}');">編輯</button></td>
										</tr>
									</c:forEach>
								</table>
							</div>
							<!-- /.box-body -->
						</div>


					</div>
					<!-- left column  start-->
					<!-- right column -->
					<div class="col-md-6">

						<!-- Horizontal Form -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">新增角色</h3>
							</div>
							<!-- /.box-header -->
							<!-- form start -->
							<form id="add_roleForm" class="form-horizontal">
								<div class="box-body">
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">角色代碼</label>
										<div class="col-sm-10">
											<input type="text" id="add_roleId" class="form-control"
												placeholder="ROLE_ADMIN" />
											<div id="msg_add_roleId"></div>
										</div>
									</div>
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-2 control-label">角色名稱</label>
										<div class="col-sm-10">
											<input type="text" id="add_roleName" class="form-control"
												placeholder="系統管理者" />
											<div id="msg_add_roleName"></div>
										</div>
									</div>
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-2 control-label">角色狀態</label>
										<div class="col-sm-10">
											<c:forEach var="status" items="${statuses}" varStatus="sn">
												<c:choose>
													<c:when test="${sn.index == 0 }">
														<input type="radio" name="add_roleStatus"
															checked="checked" value="${status.value}"
															class=" flat-red" />${status.title}
													</c:when>
													<c:otherwise>
														<input type="radio" name="add_roleStatus"
															value="${status.value}" class=" flat-red" />${status.title}
											</c:otherwise>
												</c:choose>

											</c:forEach>
											<div id="msg_add_roleStatus"></div>
										</div>
									</div>
								</div>
								<!-- /.box-body -->
								<div class="box-footer">
									<button type="button" onclick="addRoleSend();"
										class="btn btn-info pull-right">新增角色</button>
								</div>
								<!-- /.box-footer -->

							</form>
						</div>
						<!-- /.box -->
					</div>

					<!-- /.box -->
				</div>
			</section>
		</div>

		<!-- /.content-wrapper -->
		<jsp:include page="/templeates/footer.jsp" />

		<jsp:include page="/templeates/metaImport.jsp" />
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>


	<!--  edit role form start-->

	<div class="modal fade" tabindex="-1" role="dialog" id="edit_RoleModal">
		<div class="example-modal" tabindex="-1" role="dialog">
			<div class="modal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title">編輯角色</h4>
						</div>
						<div class="modal-body">
							<div class="box box-info">
								<!-- form start -->
								<form id="edit_roleForm" class="form-horizontal">
									<input type="hidden" id="edit_id" value="" />
									<div class="box-body">
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-2 control-label">角色代碼</label>
											<div class="col-sm-10">
												<input type="text" id="edit_roleId" class="form-control"
													placeholder="ROLE_ADMIN" disabled="disabled" />
												<div id="msg_edit_roleId"></div>
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">角色名稱</label>
											<div class="col-sm-10">
												<input type="text" id="edit_roleName" class="form-control"
													placeholder="系統管理者" disabled="disabled" />
												<div id="msg_edit_roleName"></div>
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">角色狀態</label>
											<div class="col-sm-10">
												<c:forEach var="status" items="${statuses}" varStatus="sn">

													<input type="radio" name="edit_roleStatus"
														value="${status.value}" class=" flat-red"
														disabled="disabled" />${status.title}
																															
											</c:forEach>
												<div id="msg_edit_roleStatus"></div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left"
								data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary"
								onclick="editRoleSend();">Save changes</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
		</div>
		<!-- /.example-modal -->
	</div>


	<!--  edit role form end -->



	<!-- ./wrapper -->
	<script type="text/javascript">
		function getRoleToEdit(id) {
			var prefix = 'edit_';
			var radioValue;
			$.ajax({
				url : '<c:url value="api/adminRole/get/'+id+'"/>',
				type : 'get',
				contentType : "application/json; charset=utf-8",
				success : function(result) {
					if (result.exeCode == 1) {
						$("#" + prefix + 'id').val(result.obj.id);
						$("#" + prefix + 'roleId').val(result.obj.roleId);
						$("#" + prefix + 'roleName').val(result.obj.roleName);
						$("#" + prefix + 'roleName').removeAttr('disabled');
						var radioValue = $(
								'input[name=edit_roleStatus]:checked',
								'#edit_roleForm').val();
						if (radioValue != result.obj.status) {
							$("input[name=edit_roleStatus]").prop('checked',
									false);
							$(
									"input[name=edit_roleStatus][value='"
											+ result.obj.status + "']").prop(
									'checked', true);
							radioEnable('edit_roleForm');
						}

						$('#edit_RoleModal').modal('show');
					} else if (result.exeCode == 0) {
						alert(result.msg);
					}
				}

			});
		}
		function editRoleSend() {
			var prefix = 'edit_';
			var editRole = {
				id : $("#" + prefix + "id").val(),
				roleName : $("#" + prefix + "roleName").val(),
				status : $(
						"#" + prefix + "roleForm input[type='radio']:checked")
						.val()
			}

			$.ajax({
				url : '<c:url value="api/adminRole/update"/>',
				type : 'put',
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(editRole),
				success : function(result) {
					if (result.exeCode == 1) {
						cleanInputValueById(prefix);
						$("tr[id^='role_list_']").remove();

						$.each(result.datas, function(index, role) {
							appendTable('role_table', index, role);
						});
						$('#edit_RoleModal').modal('hide');
						modalShow('success', result.successMsg);
					} else if (result.exeCode == 0) {
						$("div[id^='msg_edit_']").empty();
						$.each(result.errorMsgs, function(index, msg) {
							$("#msg_edit_" + msg.code).html(
									'<font color=\"red\">' + msg.value
											+ '</font>');
						});
					}
				}

			});
		}

		function addRoleSend() {
			var prefix = 'add_';
			var newRole = {
				roleId : $("#" + prefix + "roleId").val(),
				roleName : $("#" + prefix + "roleName").val(),
				status : $("#" + prefix + "roleForm input[type='radio']:checked").val()
			}

			$.ajax({
				url : '<c:url value="api/adminRole/add"/>',
				type : 'post',
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(newRole),
				success : function(result) {
					if (result.exeCode == 1) {
						cleanInputValueById(prefix);
						$("tr[id^='role_list_']").remove();

						$.each(result.datas, function(index, role) {
							appendTable('role_table', index, role);
						});
						modalShow('success', result.successMsg);
					} else if (result.exeCode == 0) {
						$("div[id^='msg_"+prefix+"']").empty();
						$.each(result.errorMsgs, function(index, msg) {
							$("#msg_add_" + msg.code).html(
									'<font color=\"red\">' + msg.value
											+ '</font>');
						});
					}
				}

			});
		}
		function appendTable(id, index, role) {
			var tr = '';
			tr = tr + '<tr id=\"role_list_'+index+'\">';
			tr = tr + '<td>' + (index + 1) + '</td>';
			tr = tr + '<td>' + role.roleId + '</td>';
			tr = tr + '<td>' + role.roleName + '</td>';
			tr = tr + '<td>' + role.showStatus + '</td>';
			tr = tr
					+ '<td><button class=\"btn btn-block btn-sm\" onclick=\"getRoleToEdit('
					+ role.id + ');\" >編輯</button></td>';
			tr = tr + '</tr>';
			$('#' + id + ' tr:last').after(tr);

		}
	</script>

</body>

</html>
