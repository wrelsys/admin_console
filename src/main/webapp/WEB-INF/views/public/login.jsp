<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>WREL Admin</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet"
	href="<c:url value="resources/plugins/bootstrap/css/bootstrap.min.css"/>">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet"
	href="<c:url value="resources/css/AdminLTE.min.css"/>">
<!-- iCheck -->
<link rel="stylesheet"
	href="<c:url value="resources/plugins/iCheck/square/blue.css"/>">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>


<body class="hold-transition login-page">
	<form id="form1" action="<c:url value="j_spring_security_check"/>"
		method="post">
		<div class="login-box">
			<div class="login-logo">
				<a href="../../index2.html"><b>WREL Admin</b></a>
			</div>
			<!-- /.login-logo -->
			<div class="login-box-body">

				<p class="login-box-msg">Sign in to start your session</p>
				<div class="form-group has-feedback">
					<input type="email" name="username" class="form-control"
						placeholder="Email"> <span
						class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" name="password" class="form-control"
						placeholder="Password"> <span
						class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>

				<div class="row">
					<div class="col-xs-8"></div>
					<!-- /.col -->
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Sign
							In</button>
					</div>
					<!-- /.col -->
				</div>


				<a href="#">I forgot my password</a><br> <a
					href="<c:url value="register"/>" class="text-center">Register a
					new membership</a>

			</div>
			<!-- /.login-box-body -->
		</div>
		<!-- /.login-box -->
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />

		<jsp:include page="/templeates/modal.jsp" />

	</form>
	<!-- jQuery 2.1.4 -->
	<script src="<c:url value="resources/js/jQuery-2.1.4.min.js"/>"></script>
	<!-- Bootstrap 3.3.5 -->
	<script
		src="<c:url value="resources/plugins/bootstrap/js/bootstrap.min.js"/>"></script>
	<!-- iCheck -->
	<script src="<c:url value="resources/plugins/iCheck/icheck.min.js"/>"></script>
	<script src="<c:url value="resources/js/wrel.js"/>"></script>
	<script>
		$(function() {
			$('input').iCheck({
				checkboxClass : 'icheckbox_square-blue',
				radioClass : 'iradio_square-blue',
				increaseArea : '20%' // optional
			});

			var msg = '${msg}';
			if (msg != null && msg != '') {
				modalShow('${type}', msg);
			}
		});
	</script>
</body>


</body>

</html>

