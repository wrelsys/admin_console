<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/templeates/meta.jsp" />

</head>

<body class="hold-transition register-page">

	<div class="register-box">
		<div class="register-logo">
			<a href="<c:url value="index"/>"><b>WREL</b>Admin</a>
		</div>

		<div class="register-box-body">
			<p class="login-box-msg">Register a new membership</p>
			<form id="registerForm">
				<div class="form-group has-feedback">
					<input type="text" id="register_name" name="name"
						class="form-control" placeholder="Full name" maxlength="5" /> <span
						class="glyphicon glyphicon-user form-control-feedback"></span>
					<div id="msg_name"></div>
				</div>
				<div class="form-group has-feedback">
					<input type="text" id="register_email" name="email"
						class="form-control" maxlength="35" placeholder="E-mail" /> <span
						class="glyphicon glyphicon-envelope form-control-feedback"></span>
					<div id="msg_email"></div>
				</div>


				<div class="form-group has-feedback">
					<div class="input-group">
						<input type="text" id="register_tel" name="tel"
							class="form-control" placeholder="Tel"> <span
							class="input-group-addon"> <i class="fa fa-phone"></i>
						</span>
					</div>
					<div id="msg_tel"></div>
					<!-- /.input group -->
				</div>
				<div class="form-group has-feedback">
					<div class="input-group">

						<input type="text" id="register_mobile" name="mobile"
							class="form-control" placeholder="Mobile">
						<div class="input-group-addon">
							<i class="fa fa-phone"></i>
						</div>
					</div>
					<div id="msg_mobile"></div>
					<!-- /.input group -->
				</div>
				<div class="form-group has-feedback">
					<input type="password" name="password" id="register_password"
						class="form-control" placeholder="password (length:8~15)">
					<span class="glyphicon glyphicon-log-in form-control-feedback"></span>
					<div id="msg_password"></div>

				</div>
				<div class="form-group has-feedback">
					<input type="password" name="rePassword" id="register_rePassword"
						class="form-control" placeholder="password (length:8~15)">

					<span class="glyphicon glyphicon-log-in form-control-feedback"></span>
					<div id="msg_rePassword"></div>
				</div>

				<div class="row">

					<!-- /.col -->
					<div class="col-xs-4">
						<button type="button" class="btn btn-primary btn-block btn-flat"
							onclick="send();">Register</button>
					</div>
					<!-- /.col -->
				</div>
				<jsp:include page="/templeates/modal.jsp" />
			</form>

			<br /> <a href="<c:url value="login"/>" class="text-center">I
				already have a membership</a>
		</div>
		<!-- /.form-box -->
	</div>
	<!-- /.register-box -->

	<!-- jQuery 2.1.4 -->
	<script src="<c:url value="resources/js/jQuery-2.1.4.min.js"/>"></script>
	<!-- Bootstrap 3.3.5 -->
	<script
		src="<c:url value="resources/plugins/bootstrap/js/bootstrap.min.js"/>"></script>
	<!-- iCheck -->
	<script src="<c:url value="resources/plugins/iCheck/icheck.min.js"/>"></script>
	<script src="<c:url value="resources/js/wrel.js"/>"></script>
	<script>
		$(function() {
			$('input').iCheck({
				checkboxClass : 'icheckbox_square-blue',
				radioClass : 'iradio_square-blue',
				increaseArea : '20%' // optional
			});
		});
	</script>

	<script type="text/javascript">
		function send() {
			var prefix = 'register_';
			var register = {
				name : $("#" + prefix + "name").val(),
				email : $("#" + prefix + "email").val(),
				tel : $("#" + prefix + "tel").val(),
				mobile : $("#" + prefix + "mobile").val(),
				password : $("#" + prefix + "password").val(),
				rePassword : $("#" + prefix + "rePassword").val()

			}

			$.ajax({
				url : '<c:url value="api/register/add"/>',
				type : 'post',
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(register),
				success : function(result) {
					if (result.exeCode == 1) {
						cleanInputValueById("register");
						
						modalShow('success', result.successMsg);
					} else if (result.exeCode == 0) {
						$("div[id^='msg_']").empty();
						$.each(result.errorMsgs, function(index, msg) {
							$("#msg_" + msg.code).html(
									'<font color=\"red\">' + msg.value
											+ '</font>');
						});
					}
				}

			});
		}
	</script>



</body>


</body>

</html>

