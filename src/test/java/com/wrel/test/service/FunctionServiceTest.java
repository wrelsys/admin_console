
package com.wrel.test.service;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.anyLong;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.wrel.admin.console.service.FunctionService;
import com.wrel.admin.console.service.impl.FunctionServiceImpl;
import com.wrel.admin.dao.FunctionDao;
import com.wrel.admin.entity.Function;
import com.wrel.admin.entity.Function.Show;
import com.wrel.admin.entity.Function.Status;
import com.wrel.common.utils.UuidUtils;
import com.wrel.test.common.MockitoTestCase;

/**
 *
 * Page/Class Name: FunctionServiceTest
 * Title:
 * Description:
 * Copyright:
 * Company:	
 * author: louis
 * Create Date:	2017年7月16日
 * Last Modifier: louis
 * Last Modify Date: 2017年7月16日
 * Version 1.0
 *
 */

public class FunctionServiceTest extends MockitoTestCase {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End
    //================================================
    //== [static variables] Block Start
    //====

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionServiceTest.class);

    //====
    //== [static variables] Block Stop
    //================================================
    //== [instance variables] Block Start

    /** The function dao. */
    @Mock
    private FunctionDao functionDao;

    /** The function service. */
    @InjectMocks
    private FunctionService functionService = new FunctionServiceImpl();

    //====
    //== [instance variables] Block Stop
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop
    //================================================
    //== [Constructors] Block Start (含init method)

    /**
     * Inits the.
     */
    //====
    @Before
    public void init() {

        MockitoAnnotations.initMocks(this);

    }

    //====
    //== [Constructors] Block Stop
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop
    //================================================
    //== [Method] Block Start
    /**
     * Test get all function.
     */
    //====
    @Test
    public void testGetAllFunction() {
        List<Function> functions1 = this.buildFunctions();

        // Mockito.when(functionDao.getAllFunctions(true)).thenReturn(functions1);
        Mockito.doReturn(functions1).when(functionDao).getAllFunctions(true);

        List<Function> functions = this.functionService.getAllFunctions();

        LOGGER.debug("functions :{}", new Gson().toJson(functions));
        Assert.assertThat(functions, hasSize(3));
    }

    /**
     * Test get functio by id.
     */
    @Test
    public void testGetFunctioById() {
        Assert.assertEquals("testGetFunctioById", super.testName.getMethodName());
        final Long functionId = 1L;

        Mockito.when(functionDao.getFunctionById(anyLong())).thenReturn(this.getFunction(functionId));
        Function function = this.functionService.getFunctionById(functionId);
        Assert.assertEquals(functionId, function.getId());

    }
    //####################################################################
    //## [Method] sub-block :

    /**
     * Builds the functions.
     *
     * @return the list
     */
    private List<Function> buildFunctions() {
        final List<Function> functions = new ArrayList<>();
        functions.add(this.getFunction(null));
        functions.add(this.getFunction(null));
        functions.add(this.getFunction(null));
        return functions;
    }

    /**
     * Gets the function.
     *
     * @param id the id
     * @return the function
     */
    private Function getFunction(Long id) {
        final String uuid = UuidUtils.getUUID();
        Function function = new Function(//
                RandomStringUtils.random(10, uuid), //
                RandomStringUtils.random(20, uuid), //
                Show.YES.isValue(), Status.ACTIVE.getValue(), 1, null);

        if (id != null) {
            function.setId(id);
        }
        return function;
    }
    //####################################################################
    //====
    //== [Method] Block Stop
    //================================================
}
