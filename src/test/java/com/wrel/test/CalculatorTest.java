package com.wrel.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.wrel.admin.console.common.Calculator;
import com.wrel.admin.console.common.SubCalculator;

/**
 * Created by TCFeng on 2017/7/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class CalculatorTest {

    @Mock
    private SubCalculator subCalculator;

    @InjectMocks
    private Calculator calculator;

    @Before
    public void setUp() {
        this.calculator = new Calculator();
        MockitoAnnotations.initMocks(this);

    }

    @After
    public void tearDown() {
        calculator = null;
    }

    @Test
    @Ignore
    public void testMulti() {

        //SubCalculator mockedSubCalculator = mock(SubCalculator.class);

        try {
            //            Field field = calculator.getClass().getDeclaredField("subCalculator");
            //            field.setAccessible(true);
            //            field.set(calculator, mockedSubCalculator);

            when(subCalculator.multiplicate(3, 4)).thenReturn(15);
            int expected = 12;
            int result = calculator.multi(3, 4);
            assertEquals(expected, result);
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
