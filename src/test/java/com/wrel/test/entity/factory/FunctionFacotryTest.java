
package com.wrel.test.entity.factory;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.wrel.admin.console.entity.factory.FunctionFactory;
import com.wrel.admin.console.request.FunctionRequestVO;
import com.wrel.admin.entity.Function;
import com.wrel.common.utils.UuidUtils;

/**
 * The Class FunctionFacotryTest.
 */
public class FunctionFacotryTest {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop
    //================================================
    //== [Method] Block Start
    @Test
    public void testConstructor() throws NoSuchMethodException, SecurityException {
        FunctionFactory functionFactory = new FunctionFactory();
        Assert.assertNotNull(functionFactory);
    }

    /**
     * Test factory get instance.
     */
    //====
    @Test
    public void testFactoryGetInstance() {
        final FunctionRequestVO requestVO = this.createFunctionRequestVO();
        Function function = FunctionFactory.getNewFunctionInstance(requestVO);
        Assert.assertEquals(requestVO.getFunctionName(), function.getFunctionName());

        requestVO.setOrder(null);
        function = FunctionFactory.getNewFunctionInstance(requestVO);
        Assert.assertEquals(0, function.getOrder());

    }
    //####################################################################
    //## [Method] sub-block :

    /**
     * Creates the function request VO.
     *
     * @return the function request VO
     */
    private FunctionRequestVO createFunctionRequestVO() {
        final String uuid = UuidUtils.getUUID();
        final FunctionRequestVO functionRequestVO = new FunctionRequestVO();
        functionRequestVO.setFunctionName(RandomStringUtils.random(10, uuid));
        functionRequestVO.setFunctionUrl(RandomStringUtils.random(20, uuid));
        functionRequestVO.setOrder("1");
        functionRequestVO.setParentId(1L);
        functionRequestVO.setShow("YES");
        functionRequestVO.setStatus(Function.Status.ACTIVE.getValue());
        return functionRequestVO;
    }
    //####################################################################
    //====
    //== [Method] Block Stop
    //================================================
}
