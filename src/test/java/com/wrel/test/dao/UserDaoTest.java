
package com.wrel.test.dao;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.wrel.admin.dao.UserDao;
import com.wrel.admin.entity.User;
import com.wrel.test.common.AbstractSpringTestCase;

/**
 *
 * Page/Class Name: UserDaoTest
 * Title:
 * Description:
 * Copyright:
 * Company:	
 * author: louis
 * Create Date:	2017年7月10日
 * Last Modifier: louis
 * Last Modify Date: 2017年7月10日
 * Version 1.0
 *
 */
public class UserDaoTest extends AbstractSpringTestCase {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End
    //================================================
    //== [static variables] Block Start
    /** The logger. */
    //====
    private final Logger LOGGER = LoggerFactory.getLogger(UserDaoTest.class); 

    //====
    //== [static variables] Block Stop
    //================================================
    //== [instance variables] Block Start
    /** The user dao. */
    //====
    @Autowired
    private UserDao userDao;

    private User user = this.createUser();

    //====
    //== [instance variables] Block Stop
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop
    //================================================
    //== [Constructors] Block Start (含init method)

    //====
    //== [Constructors] Block Stop
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop
    //================================================
    //== [Method] Block Start
    /**
     * Test inser user.
     */
    //====
    @Test
    @Rollback(true)
    public void testInserUser() {
        User user = this.createUser();
        this.userDao.save(user);
        Assert.assertNotNull(user.getId());
        LOGGER.debug(user.toString());
    }

    /**
     * Test select.
     */
    @Test
    @Rollback(true)
    public void testSelectUserByEmailAndStatus() {
        this.userDao.save(user);
        User user = this.userDao.getUserByEmailAndStatus("test123@gmail.com", User.Status.ACTIVE);
        LOGGER.debug(user.toString());
        Assert.assertNotNull(user);
        Assert.assertEquals("test123", user.getName());
    }

    //####################################################################
    /**
     * Creates the user.
     *
     * @return the user
     */
    //## [Method] sub-block :
    private User createUser() {
        User user = new User();
        user.setName("test123");
        user.setEmail("test123@gmail.com");
        user.setMobile("097565433");
        user.setPassword("ddddd");
        user.setStartCreateDate(new Date());
        user.setTel("062345432");
        user.setStatus(User.Status.ACTIVE.getValue());
        user.setModifyTime(new Date());
        user.setCreateTime(new Date());
        return user;
    }
    //####################################################################
    //====
    //== [Method] Block Stop
    //================================================
}
