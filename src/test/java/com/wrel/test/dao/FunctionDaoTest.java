
package com.wrel.test.dao;

import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.wrel.admin.console.service.FunctionService;
import com.wrel.admin.dao.FunctionDao;
import com.wrel.admin.entity.Function;
import com.wrel.test.common.AbstractSpringTestCase;

/**
 *
 * Page/Class Name: FunctionDao
 * Title:
 * Description:
 * Copyright: 
 * Company:	
 * author: 10503305 
 * Create Date:	2016年4月9日
 * Last Modify Date: 2016年4月9日
 * Version 1.0
 *
 */
public abstract class FunctionDaoTest  {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End 
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop 
    //================================================
    //== [instance variables] Block Start
    //====
//    @Autowired
//    private FunctionService functionService;

//    @Autowired
//    private FunctionDao functionDao;

    //====
    //== [instance variables] Block Stop 
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop 
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop 
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop 
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop 
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop 
    //================================================
    //== [Method] Block Start
    //====
//    @Test
//    @Rollback(false)
//    public void addNewFunction() {
//
//        final Function parent = this.functionDao.get(Function.class, 8);
//        final Function function = new Function();
//        function.setFunctionName("測試123");
//        function.setFunctionUrl("/url");
//        function.setModifyDate(new Date());
//        function.setCreateDate(new Date());
//        function.setOrder(2);
//        function.setStatus(1);
//        function.setParent(parent);
//       // this.functionService.insertFunction(function);
//    }
//
//    @Test
//    @Rollback(false)
//    public void getFunction() {
//
//        final Function function = this.functionDao.get(Function.class, 8);
//        for (Function subFunction : function.getSubFunctions()) {
//            System.out.println(subFunction.getFunctionName());
//        }
//    }
    //####################################################################
    //## [Method] sub-block : 
    //####################################################################
    //====
    //== [Method] Block Stop 
    //================================================
}
