
package com.wrel.test.validator;

import static org.hamcrest.Matchers.hasSize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wrel.admin.console.common.MsgVO;
import com.wrel.admin.console.entity.factory.FunctionFactory;
import com.wrel.admin.console.request.FunctionRequestVO;
import com.wrel.admin.console.validator.FunctionRequestValidator;
import com.wrel.admin.entity.Function;
import com.wrel.admin.entity.Function.Show;
import com.wrel.admin.entity.Function.Status;
import com.wrel.common.utils.UuidUtils;

/**
 *
 * Page/Class Name: FunctionRequestValidatorTest
 * Title:
 * Description:
 * Copyright:
 * Company:	
 * author: louis
 * Create Date:	2017年7月16日
 * Last Modifier: louis
 * Last Modify Date: 2017年7月16日
 * Version 1.0
 *
 */
public class FunctionRequestValidatorTest {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End
    //================================================
    //== [static variables] Block Start
    //====

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionRequestValidatorTest.class);

    //====
    //== [static variables] Block Stop
    //================================================
    //== [instance variables] Block Start
    /** The function request VO. */
    //====
    private FunctionRequestVO functionRequestVO;

    /** The empty msg. */
    private Map<String, String> emptyMsg = new HashMap<>();

    /** The error msg. */
    private Map<String, String> errorMsg = new HashMap<>();
    
    //====
    //== [instance variables] Block Stop
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop
    //================================================
    //== [Constructors] Block Start (含init method)
    /**
     * Inits the.
     */
    //====
    @Before
    public void init() {
        emptyMsg.put("functionName", "功能名稱不能為空");
        emptyMsg.put("functionStatus", "請選擇功能狀態");
         emptyMsg.put("id", "查無該功能");
        emptyMsg.put("show", "請選擇功能是否呈現"); 
        //functionRequestVO = new FunctionRequestVO();
        
        errorMsg.put("functionName", "功能名稱長度不能超過20");
        errorMsg.put("functionUrl", "功能URL長度不能超過50");
        errorMsg.put("functionStatus", "功能狀態錯誤，請重新選擇");
        errorMsg.put("order", "排序欄位必須為數字");
        errorMsg.put("show", "請選擇功能是否呈現"); 

    }
    //====
    //== [Constructors] Block Stop
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop
    //================================================
    //== [Method] Block Start
    //====

    /**
     * Test function request is null.
     */
    @Test
    public void testFunctionRequestIsNull() {
        this.functionRequestVO = new FunctionRequestVO();
        final List<MsgVO> msgVOs = FunctionRequestValidator.validate(functionRequestVO);
        LOGGER.debug("msg vo size :{}", CollectionUtils.size(msgVOs));
        Assert.assertThat(msgVOs, hasSize(3));
        for (MsgVO msgVO : msgVOs) {
            LOGGER.debug("msg vo :{}", msgVO.toString());
            Assert.assertEquals(msgVO.getValue(), this.emptyMsg.get(msgVO.getCode()));
        }

    }
    
    /**
     * Test function request too long.
     */
    @Test
    public void testFunctionRequestTooLong(){
        this.functionRequestVO = this.createFunctionRequestForLong();
        final List<MsgVO> msgVOs = FunctionRequestValidator.validate(functionRequestVO);
        LOGGER.debug("msg vo size :{}", CollectionUtils.size(msgVOs));
        Assert.assertThat(msgVOs, hasSize(5));
        for (MsgVO msgVO : msgVOs) {
            LOGGER.debug("msg vo :{}", msgVO.toString());
            Assert.assertEquals(msgVO.getValue(), this.errorMsg.get(msgVO.getCode()));
        }
    }
    
    /**
     * Test function request correct.
     */
    @Test
    public void testFunctionRequestCorrect(){
        this.functionRequestVO = createFunctionRequestVO();
        final List<MsgVO> msgVOs = FunctionRequestValidator.validate(functionRequestVO);
        LOGGER.debug("msg vo size :{}", CollectionUtils.size(msgVOs));
        Assert.assertThat(msgVOs, hasSize(0));
    }
    
   
    @Test
    public void testFunctionRequestUpdateIsNull(){
        this.functionRequestVO = new FunctionRequestVO();
        List<MsgVO> msgVOs = FunctionRequestValidator.updateValidate(this.functionRequestVO, null);
        LOGGER.debug("msg vo size :{}", CollectionUtils.size(msgVOs));
        Assert.assertThat(msgVOs, hasSize(4));
        for (MsgVO msgVO : msgVOs) {
            LOGGER.debug("msg vo :{}", msgVO.toString());
            Assert.assertEquals(msgVO.getValue(), this.emptyMsg.get(msgVO.getCode()));
        }

    }
    
    @Test
    public void testFunctionRequestUpdateTooLong(){
        this.functionRequestVO = this.createFunctionRequestForLong();
        final List<MsgVO> msgVOs = FunctionRequestValidator.updateValidate(functionRequestVO,this.getFunction());
        LOGGER.debug("msg vo size :{}", CollectionUtils.size(msgVOs));
        Assert.assertThat(msgVOs, hasSize(5));
        for (MsgVO msgVO : msgVOs) {
            LOGGER.debug("msg vo :{}", msgVO.toString());
            Assert.assertEquals(msgVO.getValue(), this.errorMsg.get(msgVO.getCode()));
        }
    }
    
    @Test
    public void testFunctionRequestUpdateCorrect(){
        this.functionRequestVO = createFunctionRequestVO();
        final List<MsgVO> msgVOs = FunctionRequestValidator.updateValidate(functionRequestVO, this.getFunction());
        LOGGER.debug("msg vo size :{}", CollectionUtils.size(msgVOs));
        Assert.assertThat(msgVOs, hasSize(0));
    }
    //####################################################################
    /**
     * Creates the function request for long.
     *
     * @return the function request VO
     */
    //## [Method] sub-block :
    private  FunctionRequestVO createFunctionRequestForLong(){
        final String uuid = UuidUtils.getUUID();
        final FunctionRequestVO functionRequestVO = this.createFunctionRequestVO();
        functionRequestVO.setFunctionName(RandomStringUtils.random(21, uuid));
        functionRequestVO.setFunctionUrl(RandomStringUtils.random(51, uuid));
        functionRequestVO.setStatus(2);
        functionRequestVO.setOrder("a");
        functionRequestVO.setShow("N/A");
        return functionRequestVO;
    }
    
    
    
    /**
     * Creates the function request VO.
     *
     * @return the function request VO
     */
    private FunctionRequestVO createFunctionRequestVO() {
        final String uuid  = UuidUtils.getUUID();
        final FunctionRequestVO functionRequestVO = new FunctionRequestVO();
        functionRequestVO.setFunctionName(RandomStringUtils.random(10, uuid));
        functionRequestVO.setFunctionUrl(RandomStringUtils.random(20, uuid));
        functionRequestVO.setOrder("1");
        functionRequestVO.setParentId(1L);
        functionRequestVO.setShow("YES");
        functionRequestVO.setStatus(Function.Status.ACTIVE.getValue());
        return functionRequestVO;
    }
    
    private Function getFunction(){
        final String uuid  = UuidUtils.getUUID();        
        return new Function(//
                RandomStringUtils.random(10, uuid), //
                RandomStringUtils.random(20, uuid), //
                Show.YES.isValue(), Status.ACTIVE.getValue(), 1, null);
       
    }
    
    //####################################################################
    //====
    //== [Method] Block Stop
    //================================================
}
