
package com.wrel.test.common.controller.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.mockito.internal.matchers.Contains;
import org.springframework.http.MediaType;

import com.google.gson.Gson;
import com.wrel.admin.console.request.RegisterRequestVO;
import com.wrel.test.common.AbstractSpringTestCase;

public class RegisterControllerTest extends AbstractSpringTestCase {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop
    //================================================
    //== [Method] Block Start
    //====
    @Test
    public void testRegisterPage() throws Exception {

        this.mockMvc.perform(get("/register")).andExpect(status().isOk()).andReturn(); 
        

    }

    @Test
    public void testRegisterPost() throws Exception {
        final Gson gson = new Gson();
        final RegisterRequestVO requsetVO = new RegisterRequestVO();
        requsetVO.setEmail("test123@gmail.com");
        requsetVO.setMobile("0987654323");
        requsetVO.setName("ttest");

        this.mockMvc.perform(
                post("/api/register/add").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(requsetVO)))
                .andExpect(status().isOk());
    }
    //####################################################################
    //## [Method] sub-block :
    //####################################################################
    //====
    //== [Method] Block Stop
    //================================================
}
