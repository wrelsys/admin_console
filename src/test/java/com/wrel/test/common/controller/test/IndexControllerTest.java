
package com.wrel.test.common.controller.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;

import com.wrel.test.common.AbstractSpringTestCase;

/**
 *
 * Page/Class Name: IndexControllerTest
 * Title:
 * Description:
 * Copyright:
 * Company:	
 * author: louis
 * Create Date:	2017年7月18日
 * Last Modifier: louis
 * Last Modify Date: 2017年7月18日
 * Version 1.0
 *
 */
public class IndexControllerTest extends AbstractSpringTestCase {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop
    //================================================
    //== [Method] Block Start
    @Test
    public void testIndex() throws Exception {
        this.mockMvc.perform(get("/")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(get("/?error=2")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(get("/?registed=2")).andExpect(status().isOk()).andReturn();
    }

    
    
    @Test
    @WithUserDetails("eldar.syu@gmail.com")
    public void testWelcome() throws Exception {
        this.mockMvc.perform(get("/welcome")).andExpect(status().isOk()).andReturn();

    }
    //====
    //####################################################################
    //## [Method] sub-block :
    //####################################################################
    //====
    //== [Method] Block Stop
    //================================================
}
