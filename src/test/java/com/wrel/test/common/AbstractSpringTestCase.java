package com.wrel.test.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.wrel.admin.common.LoginInfo;
import com.wrel.admin.config.AopConfig;
import com.wrel.admin.config.DataBaseConfig;
import com.wrel.admin.console.config.SecurityConfig;
import com.wrel.admin.console.config.SecurityWebApplicationInitializer;
import com.wrel.admin.console.config.SpringRootConfig;
import com.wrel.admin.console.config.SpringWebConfig;
import com.wrel.admin.console.config.WebInitializer;
import com.wrel.admin.entity.User;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { SpringWebConfig.class, SpringRootConfig.class, AopConfig.class, DataBaseConfig.class,
                                  SecurityConfig.class, SecurityWebApplicationInitializer.class, WebInitializer.class })
@EnableTransactionManagement
@Transactional
public abstract class AbstractSpringTestCase extends BaseTestCase {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    FilterChainProxy springSecurityFilterChain;

    protected MockMvc mockMvc;

    @Before
    public void setup() {

        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)//
                .apply(SecurityMockMvcConfigurers.springSecurity(springSecurityFilterChain)).build();

        MockitoAnnotations.initMocks(this);
    }
    

    //    @Test
    //    public void test_welcome() throws Exception {
    //
    //        MvcResult result = this.mockMvc.perform(get("/hello/mkyong")).andExpect(status().isOk()).andReturn();
    //
    //    }

}