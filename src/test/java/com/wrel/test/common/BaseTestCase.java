
package com.wrel.test.common;

import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class BaseTestCase {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop
    //================================================
    //== [Constructors] Block Start (含init method)
    //====

    @Rule
    public TestName testName = new TestName();

    @Rule
    public TestWatcher testWatcher = new TestWatcher() {
        @Override
        protected void starting(final Description description) {
            String methodName = description.getMethodName();
            String className = description.getClassName();
            className = className.substring(className.lastIndexOf('.') + 1);
            System.err.println("Starting JUnit-test: " + className + " " + methodName);
        }
    };

    //====
    //== [Constructors] Block Stop
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block :
    //####################################################################
    //====
    //== [Method] Block Stop
    //================================================
}
