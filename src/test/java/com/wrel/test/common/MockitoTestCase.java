
package com.wrel.test.common;

import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * Page/Class Name: MockitoTestCase
 * Title:
 * Description:
 * Copyright:
 * Company:	
 * author: louis
 * Create Date:	2017年7月11日
 * Last Modifier: louis
 * Last Modify Date: 2017年7月11日
 * Version 1.0
 *
 */
@RunWith(MockitoJUnitRunner.class)
public abstract class MockitoTestCase extends BaseTestCase {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End
    //================================================
    //== [static variables] Block Start
    //====
    //====
    //== [static variables] Block Stop
    //================================================
    //== [instance variables] Block Start
    /** The session factory. */
    //====

    //====
    //== [instance variables] Block Stop
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop
    //================================================
    //== [Constructors] Block Start (含init method)
    //====

    //====
    //== [Constructors] Block Stop
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    //====
    //== [Overrided Method] Block Stop
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    /**
     * Sets the up dao mock.
     *
     * @param <T> the generic type
     * @param obj the new up dao mock
     */
    //## [Method] sub-block :
    protected <T> void setUpDaoMock(T obj) {

        //        Mockito.when(this.sessionFactory.getCurrentSession()).thenReturn(this.session);
        //        Mockito.when(this.session.createQuery(Mockito.anyString())).thenReturn(this.query);
        //        Mockito.doReturn(this.session).when(this.sessionFactory).getCurrentSession();
        //        Mockito.doReturn(this.query).when(this.session).createQuery(Mockito.anyString());
        //        Mockito.doReturn(obj).when(this.query).list();

        // Mockito.doReturn(obj).when(this.query).uniqueResult();
        //
    }
    //####################################################################
    //====
    //== [Method] Block Stop
    //================================================
}
