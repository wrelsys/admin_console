DROP TABLE IF EXISTS `wrel_users`;
CREATE TABLE `wrel_users` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT ' key',
	`email` VARCHAR(100) NOT NULL COMMENT 'email',
	`name` VARCHAR(15) NOT NULL COMMENT '姓名',
	`password` VARCHAR(100) NOT NULL COMMENT 'password',
	`status` INT(1) NOT NULL COMMENT '狀態 0：停用 1：啟用  2:待驗證',
	`create_time` DATETIME NOT NULL COMMENT '建立時間',
	`modify_time` DATETIME NOT NULL COMMENT '修改時間',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=2
;


-- ----------------------------
-- init sql for wrel_users
-- ----------------------------
INSERT INTO `wrel_users` (email, name, password, status, create_time, modify_time) VALUES ('eldar.syu@gmail.com', '', 'gnzLDuqKcGxMNKFokfhOew==', 1, now(), now());

DROP TABLE IF EXISTS `wrel_functions`;
CREATE TABLE `wrel_functions` (
	`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'key',
	`function_name` VARCHAR(50) NOT NULL COMMENT '功能名稱',
	`function_url` VARCHAR(50) NOT NULL COMMENT '功能url',
	`parent_id` INT(11) NULL DEFAULT NULL COMMENT '父功能 id',
	`order` INT(11) NULL DEFAULT NULL COMMENT '排序',
	`status` INT(1) NOT NULL COMMENT '狀態 1：啟用, 0：停用',
	`create_date` DATETIME NOT NULL COMMENT '建立日期',
	`modify_date` DATETIME NOT NULL COMMENT '修改日期',
	INDEX `id` (`id`),
	INDEX `FK_wrel_functions_wrel_functions` (`parent_id`),
	CONSTRAINT `FK_wrel_functions_wrel_functions` FOREIGN KEY (`parent_id`) REFERENCES `wrel_functions` (`id`)
)
COMMENT='功能資料表'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

DROP TABLE IF EXISTS `wrel_roles`;
CREATE TABLE `wrel_roles` (
	`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'key',
	`role_name` VARCHAR(10) NOT NULL COMMENT '角色名稱',
	`status` INT(1) NOT NULL COMMENT '狀態 1：啟用, 0：停用',
	`create_date` DATETIME NOT NULL COMMENT '建立日期',
	`modify_date` DATETIME NOT NULL COMMENT '修改日期',
	INDEX `id` (`id`)
)
COMMENT='角色資料表'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
DROP TABLE IF EXISTS `wrel_role_functions`;
CREATE TABLE `wrel_role_functions` (
	`role_id` INT(11) NOT NULL COMMENT '角色id',
	`function_id` INT(11) NOT NULL COMMENT '功能id',
	INDEX `FK__wrel_roles` (`role_id`),
	INDEX `FK__wrel_functions` (`function_id`),
	CONSTRAINT `FK__wrel_functions` FOREIGN KEY (`function_id`) REFERENCES `wrel_functions` (`id`),
	CONSTRAINT `FK__wrel_roles` FOREIGN KEY (`role_id`) REFERENCES `wrel_roles` (`id`)
)
COMMENT='角色與功能對應表'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
